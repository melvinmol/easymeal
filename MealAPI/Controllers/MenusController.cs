﻿using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using DomainModel;
using DomainServices.Repositories;
using Halcyon.HAL;
using Halcyon.Web.HAL;
using MealAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace MealAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenusController : Controller
    {
        private IWeekMenuDAO weekMenuDAO;
        private IMealDAO mealDAO;

        public MenusController(IWeekMenuDAO weekMenuDAO, IMealDAO mealDAO)
        {
            this.weekMenuDAO = weekMenuDAO;
            this.mealDAO = mealDAO;
        }

        /*
         * Gets all WeekMenus from DAO
         * Returns: JSON page with all WeekMenus
         */
        [HttpGet]
        public IActionResult Get()
        {
            var weekMenus = weekMenuDAO.ReadWithoutMeals().Result;
            if (weekMenus != null)
            {
                JObject json = new JObject { ["WeekMenus"] = JToken.FromObject(weekMenus) };
                var links = LinkHelper.MakeLinksMenus(weekMenus);
                return Ok(new HALResponse(json).AddLinks(links));
            }
            return NotFound();
        }

        /*
         * Get WeekMenu by id from DAO
         * Returns: JSON page with WeekMenu
         */
        [HttpGet("{menuId:int}")]
        public IActionResult Get(int menuId)
        {
            var weekMenu = weekMenuDAO.ReadById(menuId).Result;
            if (weekMenu != null)
            {
                Link[] links = LinkHelper.MakeLinksMeals(weekMenu.Meals, "/meals");
                weekMenu.Meals.Clear();
                JObject json = new JObject { ["WeekMenu"] = JToken.FromObject(weekMenu) };
                return Ok(new HALResponse(json).AddLinks(links));
            }
            return NotFound();
        }

        /*
         * Gets all Meals within a WeekMenu from DAO
         * Returns: JSON page with Meals
         */
        [HttpGet("{menuId:int}/meals")]
        public IActionResult GetMeals(int menuId)
        {
            var meals = mealDAO.ReadByWeekMenuId(menuId).Result;
            JObject json = new JObject { ["Meals"] = JToken.FromObject(meals) };
            if (meals != null)
            {
                var links = LinkHelper.MakeLinksMeals(meals, $"");
                return Ok(new HALResponse(JObject.FromObject(json)).AddLinks(links));
            }
            return NotFound();
        }

        /*
         * Gets Meal by id within a WeekMenu from DAO
         * Returns: JSON page with Meal
         */
        [HttpGet("{menuId:int}/meals/{mealId:int}")]
        public IActionResult GetMealById(int menuId, int mealId)
        {
            var meal = mealDAO.ReadById(mealId).Result;
            if (meal != null)
            {
                return Ok(new HALResponse(JObject.FromObject(meal)).AddLinks(new Link[] { new Link("methodes", "GET")}));
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Post()
        {
            return StatusCode(501);
        }

        [HttpPut]
        public IActionResult Put()
        {
            return StatusCode(501);
        }

        [HttpPatch]
        public IActionResult Patch()
        {
            return StatusCode(501);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return StatusCode(501);
        }
    }
}
