﻿using DomainServices.Repositories;
using Halcyon.HAL;
using MealAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MealAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealsController : Controller
    {
        private IMealDAO mealDAO;

        public MealsController(IMealDAO mealDAO)
        {
            this.mealDAO = mealDAO;
        }

        /*
         * Gets all meals form DAO
         * Returns: JSON page with all meals
         */
        [HttpGet]
        public IActionResult Get()
        {
            var meals = mealDAO.Read().Result;
            if (meals != null)
            {
                JObject json = new JObject { ["Meals"] = JToken.FromObject(meals) };
                Link[] links = LinkHelper.MakeLinksMeals(meals, "api/meals");
                return Ok(new HALResponse(json).AddLinks(links));
            }
            return NotFound();
        }

        /*
         * Get meal by id from DAO
         * Returns: JSON page with meal
         */
        [HttpGet("{mealId}")]
        public IActionResult Get(int mealId)
        {
            var meal = mealDAO.ReadById(mealId).Result;
            if (meal != null)
            {
                return Ok(new HALResponse(JObject.FromObject(meal)));
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Post()
        {
            return StatusCode(501);
        }

        [HttpPut]
        public IActionResult Put()
        {
            return StatusCode(501);
        }

        [HttpPatch]
        public IActionResult Patch()
        {
            return StatusCode(501);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return StatusCode(501);
        }
    }
}
