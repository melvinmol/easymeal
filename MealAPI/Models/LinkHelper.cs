﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using Halcyon.HAL;
using Microsoft.AspNetCore.Mvc.TagHelpers;

namespace MealAPI.Models
{
    public static class LinkHelper
    {
        /*
         * Creates link for every Meal
         * Param: List with Meals
         * Param: base url
         * Returns: Array with Links
         */
        public static Link[] MakeLinksMeals(IList<Meal> list, string baseUrl)
        {
            Link[] links = new Link[list.Count + 1];
            int counter = 0;
            foreach (var meal in list)
            {
                links[counter] = new Link("self", $"{baseUrl}/{meal.Id}");
                counter++;
            }
            links[list.Count] = new Link("methodes", $"GET");
            return links;
        }

        /*
         * Creates link for every WeekMenu
         * Param: List with WeekMenus
         * Param: base url
         * Returns: Array with Links
         */
        public static Link[] MakeLinksMenus(IList<WeekMenu> weekMenus)
        {
            Link[] links = new Link[weekMenus.Count + 1];
            int counter = 0;
            foreach (var weekMenu in weekMenus)
            {
                links[counter] = new Link("self", $"/{weekMenu.Id}");
                counter++;
            }
            links[weekMenus.Count] = new Link("methodes", $"GET");
            return links;
        }
    }
}
