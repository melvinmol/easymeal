﻿using DomainServices.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MealAPILevel2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealsController : Controller
    {
        private IMealDAO mealDAO;

        public MealsController(IMealDAO mealDAO)
        {
            this.mealDAO = mealDAO;
        }

        /*
         * Gets all meals form DAO
         * Returns: JSON page with all meals
         */
        [HttpGet]
        public IActionResult Get()
        {
            var meals = mealDAO.Read().Result;
            return Ok(meals);
        }

        /*
         * Get meal by id from DAO
         * Returns: JSON page with meal
         */
        [HttpGet("{mealId}")]
        public IActionResult Get(int mealId)
        {
            var meal = mealDAO.ReadById(mealId).Result;
            return Ok(meal);
        }
    }
}
