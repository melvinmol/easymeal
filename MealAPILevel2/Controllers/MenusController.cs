﻿using DomainServices.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace MealAPILevel2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenusController : Controller
    {
        private IWeekMenuDAO weekMenuDAO;
        private IMealDAO mealDAO;

        public MenusController(IWeekMenuDAO weekMenuDAO, IMealDAO mealDAO)
        {
            this.weekMenuDAO = weekMenuDAO;
            this.mealDAO = mealDAO;
        }

        /*
         * Gets all WeekMenus from DAO
         * Returns: JSON page with all WeekMenus
         */
        [HttpGet]
        public IActionResult Get()
        {
            var weekMenus = weekMenuDAO.ReadWithoutMeals().Result;
            return Ok(weekMenus);
        }

        /*
         * Get WeekMenu by id from DAO
         * Returns: JSON page with WeekMenu
         */
        [HttpGet("{menuId:int}")]
        public IActionResult Get(int menuId)
        {
            var weekMenu = weekMenuDAO.ReadById(menuId).Result;
            return Ok(weekMenu);
        }

        /*
         * Gets all Meals within a WeekMenu from DAO
         * Returns: JSON page with Meals
         */
        [HttpGet("{menuId:int}/meals")]
        public IActionResult GetMeals(int menuId)
        {
            var meals = mealDAO.ReadByWeekMenuId(menuId).Result;
            return Ok(meals);
        }

        /*
         * Gets Meal by id within a WeekMenu from DAO
         * Returns: JSON page with Meal
         */
        [HttpGet("{menuId:int}/meals/{mealId:int}")]
        public IActionResult GetMealById(int menuId, int mealId)
        {
            var meal = mealDAO.ReadById(mealId).Result;
            return Ok(meal);
        }
    }
}
