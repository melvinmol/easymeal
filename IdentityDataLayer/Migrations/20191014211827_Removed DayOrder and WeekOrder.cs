﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityDataLayer.Migrations
{
    public partial class RemovedDayOrderandWeekOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail",
                table: "CustomerDietaryRestriction");

            migrationBuilder.DropTable(
                name: "DayOrder");

            migrationBuilder.DropTable(
                name: "WeekOrder");

            migrationBuilder.AddColumn<string>(
                name: "CustomerEmail1",
                table: "CustomerDietaryRestriction",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DietaryRestriction",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DietaryRestriction", x => x.Name);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerDietaryRestriction_CustomerEmail1",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail1");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail1",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail1",
                principalTable: "Customer",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail1",
                table: "CustomerDietaryRestriction");

            migrationBuilder.DropTable(
                name: "DietaryRestriction");

            migrationBuilder.DropIndex(
                name: "IX_CustomerDietaryRestriction_CustomerEmail1",
                table: "CustomerDietaryRestriction");

            migrationBuilder.DropColumn(
                name: "CustomerEmail1",
                table: "CustomerDietaryRestriction");

            migrationBuilder.CreateTable(
                name: "WeekOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerEmail = table.Column<string>(nullable: true),
                    EndOfWeek = table.Column<DateTime>(nullable: false),
                    StartOfWeek = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeekOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WeekOrder_Customer_CustomerEmail",
                        column: x => x.CustomerEmail,
                        principalTable: "Customer",
                        principalColumn: "Email",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DayOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<int>(nullable: false),
                    WeekOrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DayOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DayOrder_WeekOrder_WeekOrderId",
                        column: x => x.WeekOrderId,
                        principalTable: "WeekOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DayOrder_WeekOrderId",
                table: "DayOrder",
                column: "WeekOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail",
                principalTable: "Customer",
                principalColumn: "Email",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
