﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityDataLayer.Migrations
{
    public partial class AddCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDietaryRestriction_AbstractPerson_CustomerEmail1",
                table: "CustomerDietaryRestriction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AbstractPerson",
                table: "AbstractPerson");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AbstractPerson");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "AbstractPerson");

            migrationBuilder.RenameTable(
                name: "AbstractPerson",
                newName: "Customer");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerNumber",
                table: "Customer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Birthday",
                table: "Customer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customer",
                table: "Customer",
                column: "Email");

            migrationBuilder.CreateTable(
                name: "Chef",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chef", x => x.Email);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail1",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail1",
                principalTable: "Customer",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail1",
                table: "CustomerDietaryRestriction");

            migrationBuilder.DropTable(
                name: "Chef");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customer",
                table: "Customer");

            migrationBuilder.RenameTable(
                name: "Customer",
                newName: "AbstractPerson");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerNumber",
                table: "AbstractPerson",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Birthday",
                table: "AbstractPerson",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AbstractPerson",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "AbstractPerson",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_AbstractPerson",
                table: "AbstractPerson",
                column: "Email");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerDietaryRestriction_AbstractPerson_CustomerEmail1",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail1",
                principalTable: "AbstractPerson",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
