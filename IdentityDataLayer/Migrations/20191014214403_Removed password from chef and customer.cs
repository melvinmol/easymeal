﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityDataLayer.Migrations
{
    public partial class Removedpasswordfromchefandcustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Chef");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Chef",
                nullable: true);
        }
    }
}
