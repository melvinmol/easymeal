﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityDataLayer.Migrations
{
    public partial class AddChefandCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Chef",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chef", x => x.Email);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    CustomerNumber = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    HouseNumber = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Email);
                });

            migrationBuilder.CreateTable(
                name: "CustomerDietaryRestriction",
                columns: table => new
                {
                    CustomerEmail = table.Column<string>(nullable: false),
                    DietaryRestrictionName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerDietaryRestriction", x => new { x.CustomerEmail, x.DietaryRestrictionName });
                    table.ForeignKey(
                        name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail",
                        column: x => x.CustomerEmail,
                        principalTable: "Customer",
                        principalColumn: "Email",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WeekOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartOfWeek = table.Column<DateTime>(nullable: false),
                    EndOfWeek = table.Column<DateTime>(nullable: false),
                    CustomerEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeekOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WeekOrder_Customer_CustomerEmail",
                        column: x => x.CustomerEmail,
                        principalTable: "Customer",
                        principalColumn: "Email",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DayOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<int>(nullable: false),
                    WeekOrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DayOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DayOrder_WeekOrder_WeekOrderId",
                        column: x => x.WeekOrderId,
                        principalTable: "WeekOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DayOrder_WeekOrderId",
                table: "DayOrder",
                column: "WeekOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Chef");

            migrationBuilder.DropTable(
                name: "CustomerDietaryRestriction");

            migrationBuilder.DropTable(
                name: "DayOrder");

            migrationBuilder.DropTable(
                name: "WeekOrder");

            migrationBuilder.DropTable(
                name: "Customer");
        }
    }
}
