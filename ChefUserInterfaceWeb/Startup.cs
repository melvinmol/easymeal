﻿using ChefUserInterfaceWeb.Models;
using DomainModel;
using DomainServices.Repositories;
using IdentityDataLayer;
using MealDataLayer;
using MealDataLayer.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ChefUserInterfaceWeb
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment env { get; }

        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            this.env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var mealDbConnectionString = Configuration["easy-meal-mealdb:ConnectionString"];
            var identityDbConnectionString = Configuration["easy-meal-identitydb:ConnectionString"];

            if (env.IsDevelopment())
            {
                mealDbConnectionString = Configuration["easy-meal-mealdbLocal:ConnectionString"];
                identityDbConnectionString = Configuration["easy-meal-identitydbLocal:ConnectionString"];
            }
            services.AddDbContext<MealDbContext>(options =>
                options.UseSqlServer(mealDbConnectionString));

            services.AddDbContext<IdentityDbContext>(options =>
                options.UseSqlServer(identityDbConnectionString));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IChefDAO, SQLChefDAO>();
            services.AddTransient<IMealDAO, SQLMealDAO>();
            services.AddTransient<IWeekMenuDAO, SQLWeekMenuDAO>();

            services.AddSession();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<MealCart>(mealCart => SessionMealCart.GetMealCart(mealCart));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}");
            });
            app.UseMvcWithDefaultRoute();
        }
    }
}
