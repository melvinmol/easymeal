﻿using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ChefUserInterfaceWeb.Models
{
    public static class AddMealHelper
    {
        public static async Task AddMeal(Meal meal, IFormFile image, IMealDAO mealDAO)
        {
            var memoryStream = new MemoryStream();
            await image.CopyToAsync(memoryStream);
            meal.Image = memoryStream.ToArray();
            await mealDAO.Create(meal);
        }
    }
}
