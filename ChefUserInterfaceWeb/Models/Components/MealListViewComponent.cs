﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ChefUserInterfaceWeb.Models.Components
{
    public class MealListViewComponent : ViewComponent
    {
        private IMealDAO mealDAO;

        public MealListViewComponent(IMealDAO mealDAO)
        {
            this.mealDAO = mealDAO;
        }

        public IViewComponentResult Invoke()
        {
            return View(mealDAO.Read().Result);
        }
    }
}
