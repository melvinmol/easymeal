﻿using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Identity;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public static class AddAccountHelper
    {
        /*
         * SignUp IdentityUser
         * Checks if IdentityCuster, if not returns false
         * Creates Chef in database from SignUpViewModel
         * Param: SignUpViewModel with all Chef attributes
         * Param: IdentityUser
         * Param: SignInManager
         * Param: ChefDAO
         * Returns: true
         */
        public static async Task<bool> AddAccount(AddAccountViewModel addAccountViewModel, UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager, IChefDAO chefDAO)
        {
            //Creates IdentityUser
            IdentityUser user = new IdentityUser(addAccountViewModel.Email);
            var createdUser = await userManager.CreateAsync(user, addAccountViewModel.Password);
            if (createdUser.Succeeded)
            {
                await chefDAO.Create(new Chef()
                {
                    Email = addAccountViewModel.Email,
                    FirstName = addAccountViewModel.FirstName,
                    SecondName = addAccountViewModel.SecondName,
                    PhoneNumber = addAccountViewModel.PhoneNumber
                });
                return true;
            }
            return false;
        }
    }
}
