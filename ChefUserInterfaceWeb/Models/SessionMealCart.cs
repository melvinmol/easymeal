﻿using System;
using DomainModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace ChefUserInterfaceWeb.Models
{
    public class SessionMealCart : MealCart
    {

        public static MealCart GetMealCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;
            SessionMealCart cart = session?.GetJson<SessionMealCart>("MealCart")
                               ?? new SessionMealCart();
            cart.Session = session;
            return cart;
        }
        [JsonIgnore]
        public ISession Session { get; set; }
        public override void AddItem(Meal meal, int quantity)
        {
            base.AddItem(meal, quantity);
            Session.SetJson("MealCart", this);
        }
        public override void RemoveLine(Meal meal)
        {
            base.RemoveLine(meal);
            Session.SetJson("MealCart", this);
        }
        public override void Clear()
        {
            base.Clear();
            Session.Remove("MealCart");
        }
    }
}
