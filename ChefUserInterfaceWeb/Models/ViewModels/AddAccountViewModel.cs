﻿using System.ComponentModel.DataAnnotations;

namespace UserInterfaceWeb.Models.ViewModels
{
    public class AddAccountViewModel
    {
        [Required(ErrorMessage = "Email moet ingevuld worden.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Voornaam moet ingevuld worden.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Achternaam moet ingevuld worden.")]
        public string SecondName { get; set; }
        [Required(ErrorMessage = "Telefoon nummer moet ingevuld worden.")]
        public string PhoneNumber { get; set; }
        [RegularExpression(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            ErrorMessage = "Een wachtwoord moet de volgende tekens bevatten: een kleine letter, een grote letter, " +
                           "een cijfer, een speciaal teken(#?!@$%^&*-) en moet minimaal 8 karakters lang zijn")]
        public string Password { get; set; }
    }
}
