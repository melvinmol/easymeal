﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UserInterfaceWeb.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Email moet ingevuld worden.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Wachtwoord moet ingevuld worden.")]
        public string Password { get; set; }
    }
}
