﻿using DomainModel;
using DomainModel.Models;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ChefUserInterfaceWeb.Models
{
    public static class AddMenuHelper
    {
        public static async Task<bool> AddMenu(WeekMenu menu, MealCart mealCart,
            IWeekMenuDAO menuDAO, IdentityUser user, IChefDAO chefDAO)
        {
            menu.Chef = await chefDAO.ReadByEmail(user.UserName);
            foreach (CartLine cartLine in mealCart.Lines)
            {
                menu.Meals.Add(cartLine.Meal);
            }
            if (AllDietsChecker.WeekMenuHasAllDiets(menu)) {
                await menuDAO.Create(menu);
                return true;
            }
            return false;
        }
    }
}
