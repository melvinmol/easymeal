﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace UserInterfaceWeb.Models
{
    public static class LoginHelper
    {
        /*
         * Login user
         * Checks is email exits in database, if not returns false
         * Checks if password is correct, if not returns false
         * Param: LoginViewModel with password and email
         * Param: UserManager
         * Param: SignInManager
         * Returns: true
         */
        public static async Task<bool> Login(LoginViewModel loginViewModel, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            IdentityUser user =
                await userManager.FindByNameAsync(loginViewModel.Email);
            if (user != null)
            {
                await signInManager.SignOutAsync();
                if ((await signInManager.PasswordSignInAsync(user,
                    loginViewModel.Password, false, false)).Succeeded)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
