﻿using System.Threading.Tasks;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserInterfaceWeb.Models;
using UserInterfaceWeb.Models.ViewModels;

namespace ChefUserInterfaceWeb.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<IdentityUser> userManager;
        private SignInManager<IdentityUser> signInManager;
        private IChefDAO chefDAO;

        public AccountController(IChefDAO chefDAO, UserManager<IdentityUser> userMgr,
            SignInManager<IdentityUser> signInMgr)
        {
            this.chefDAO = chefDAO;
            userManager = userMgr;
            signInManager = signInMgr;
        }

        /*
         * Get AddAccount page
         * Returns: AddAccount page
         */
        [Authorize]
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        /*
         * Post Chef
         * Post IdentityUser
         * Check if IdentityUser if not return error
         * Param: new Customer
         * Returns: Order Index page
         */
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(AddAccountViewModel addAccountViewModel)
        {
            if (ModelState.IsValid)
            {
                if (await AddAccountHelper.AddAccount(addAccountViewModel, userManager,
                    signInManager, chefDAO))
                {
                    return RedirectToAction("Login");
                }
                ModelState.AddModelError("SignUpFaild",
                    "Het aanmelden is niet gelukt.");
            }
            return View();
        }

        /*
         * Get Login page
         * Returns: Login page
         */
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        /*
         * Post email and password
         * Checks if ModelState is valid, if not return error
         * Checks if email and password match, if not return error
         * Param: ViewModel with email and password
         * Returns: Order Index page
         */
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                if (await LoginHelper.Login(loginViewModel, userManager, signInManager))
                {
                    return RedirectToAction("Index", "Menu");
                }
                ModelState.AddModelError("EmailNotExitsOrPasswordIncorrect",
                    "Het emailadres bestaat niet, of het wachtwoord in incorrect");
            }
            return View(loginViewModel);
        }

        /*
         * Logout user
         * Returns: Home Index page
         */
        public async Task<RedirectResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Redirect("/");
        }
    }
}
