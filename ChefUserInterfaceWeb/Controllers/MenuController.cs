﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using ChefUserInterfaceWeb.Models;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ChefUserInterfaceWeb.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        private MealCart mealCart;
        private IWeekMenuDAO weekMenuDAO;
        private IChefDAO chefDAO;
        private IMealDAO mealDAO;
        private UserManager<IdentityUser> userManager;

        public MenuController(IWeekMenuDAO weekMenuDAO, IMealDAO mealDAO, IChefDAO chefDAO,
            MealCart mealCart, UserManager<IdentityUser> userMgr)
        {
            this.weekMenuDAO = weekMenuDAO;
            this.mealDAO = mealDAO;
            this.chefDAO = chefDAO;
            this.mealCart = mealCart;
            userManager = userMgr;
        }

        public async Task<IActionResult> Index()
        {
            List<WeekMenu> weekMenus = await weekMenuDAO.Read();
            return View(weekMenus);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(WeekMenu weekMenu)
        {
            IdentityUser user = await userManager.GetUserAsync(HttpContext.User);

            if (await AddMenuHelper.AddMenu(weekMenu, mealCart, weekMenuDAO, user, chefDAO))
            {
                mealCart.Clear();
                return Redirect("Index");
            } else
            {
                mealCart.Clear();
                ModelState.AddModelError("NotAllDiets", "Er moet minimaal voor ieder dieet een maaltijd zitten in een menu");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddMealToMenu(Meal meal)
        {
            Meal dbMeal = await mealDAO.ReadById(meal.Id);
            mealCart.AddItem(dbMeal, 1);
            return Redirect("Add");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteMealFromMenu(Meal meal)
        {
            Meal dbMeal = await mealDAO.ReadById(meal.Id);
            mealCart.RemoveLine(dbMeal);
            return Redirect("Add");
        }
    }
}
