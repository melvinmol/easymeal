﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChefUserInterfaceWeb.Models;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace ChefUserInterfaceWeb.Controllers
{
    [Authorize]
    public class MealController : Controller
    {
        private IMealDAO mealDAO;

        public MealController(IMealDAO mealDAO)
        {
            this.mealDAO = mealDAO;
        }

        /**
         * Get Add page
         */
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        /**
         * Post meal
         */
        [HttpPost]
        public async Task<IActionResult> Add(Meal meal, IFormFile image)
        {
            await AddMealHelper.AddMeal(meal, image, mealDAO);
            return View();
        }
    }
}
