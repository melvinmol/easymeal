﻿$(document).ready(function () {
    addMealToMenu()
    deleteMealToMenu()
})

function addMealToMenu() {
    $(".add-meal-to-menu").click(function () {
        let meal = $(this).val().split(/[|]/)
        let mealId = meal[0]
        let mealName = meal[1]
        let mealInMenu = $(".meal-in-menu")
        let dsx = mealInMenu.children.length
        mealInMenu.append(`<tr><td> ${mealName}
            </td><td><button class='btn-orange btn btn-sm float-right delete-meal-to-menu'
            value="${mealId}"><i class='fas fa-times'>
            </i></button></td></tr>`)
        let Meal = {
            Id: mealId
        }
        $.ajax({
            type: "POST",
            url: "AddMealToMenu",
            data: Meal
        });
    })
}

function deleteMealToMenu() {
    $(".delete-meal-to-menu").click(function () {
        let mealId = $(this).val()

        let Meal = {
            Id: mealId
        }
        $.ajax({
            type: "DELETE",
            url: "DeleteMealToMenu",
            data: Meal
        });
        console.log('hi')
        $(this).remove()
    })
}