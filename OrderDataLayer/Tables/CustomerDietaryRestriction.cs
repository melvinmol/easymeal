﻿using DomainModel;

namespace OrderDataLayer.Tables
{
    public class CustomerDietaryRestriction
    {
        public string CustomerEmail { get; set; }
        public Customer Customer{ get; set; }

        public string DietaryRestrictionName { get; set; }
    }
}
