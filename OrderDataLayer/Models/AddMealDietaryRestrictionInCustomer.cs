﻿using System.Collections.Generic;
using DomainModel;
using OrderDataLayer.Tables;

namespace OrderDataLayer.Models
{
    public static class AddMealDietaryRestrictionInCustomer
    {
        public static void Execute(Customer customer, IList<CustomerDietaryRestriction> customerDietaryRestrictions)
        {
            IList<DietaryRestriction> dietaryRestrictions = new List<DietaryRestriction>();
            foreach (CustomerDietaryRestriction customerRestriction in customerDietaryRestrictions)
            {
                dietaryRestrictions.Add(new DietaryRestriction()
                {
                    Name = customerRestriction.DietaryRestrictionName
                });
            }
            customer.DietaryRestrictions = dietaryRestrictions;
        }
    }
}
