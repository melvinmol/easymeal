﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using Newtonsoft.Json;

namespace OrderDataLayer.Repositories
{
    static class RequestAPI<T>
    {
        private static HttpClient _client = new HttpClient();

        /*
         * Send a http request to a server
         * Param: the url
         * Return: the body from the requested url as an object
         */
        public static async Task<T> Execute(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                url);

            HttpResponseMessage response = await _client.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            int first = content.IndexOf(":");
            string toReplace1 = content.Substring(first + 1);

            int indexRemoveFrom = toReplace1.IndexOf("_links");


            string endResult = "";
            if (indexRemoveFrom > 0)
            {
                string toReplace = toReplace1.Substring(indexRemoveFrom - 2);
                endResult = toReplace1.Replace(toReplace, "");
            } else
            {
                endResult = toReplace1.Remove(toReplace1.Length - 1);
            }

            T result = JsonConvert.DeserializeObject<T>(endResult);
            return result;
        }
    }
}
