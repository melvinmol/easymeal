﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class AddeddayOrderIdtomealOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealOrder_DayOrder_DayOrderId",
                table: "MealOrder");

            migrationBuilder.DropColumn(
                name: "DayOrderId",
                table: "WeekOrder");

            migrationBuilder.AlterColumn<int>(
                name: "DayOrderId",
                table: "MealOrder",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MealOrder_DayOrder_DayOrderId",
                table: "MealOrder",
                column: "DayOrderId",
                principalTable: "DayOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealOrder_DayOrder_DayOrderId",
                table: "MealOrder");

            migrationBuilder.AddColumn<int>(
                name: "DayOrderId",
                table: "WeekOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "DayOrderId",
                table: "MealOrder",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_MealOrder_DayOrder_DayOrderId",
                table: "MealOrder",
                column: "DayOrderId",
                principalTable: "DayOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
