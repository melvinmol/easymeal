﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class AddedOrderMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndOfWeek",
                table: "WeekOrder");

            migrationBuilder.DropColumn(
                name: "StartOfWeek",
                table: "WeekOrder");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "MealOrder");

            migrationBuilder.AddColumn<int>(
                name: "WeekMenuId",
                table: "WeekOrder",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WeekMenuId",
                table: "WeekOrder");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndOfWeek",
                table: "WeekOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartOfWeek",
                table: "WeekOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "MealOrder",
                nullable: false,
                defaultValue: 0);
        }
    }
}
