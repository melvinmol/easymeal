﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class Addcustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CustomerEmail",
                table: "WeekOrder",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    CustomerNumber = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    HouseNumber = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Email);
                });

            migrationBuilder.CreateTable(
                name: "CustomerDietaryRestriction",
                columns: table => new
                {
                    CustomerEmail = table.Column<string>(nullable: false),
                    DietaryRestrictionName = table.Column<string>(nullable: false),
                    CustomerEmail1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerDietaryRestriction", x => new { x.CustomerEmail, x.DietaryRestrictionName });
                    table.ForeignKey(
                        name: "FK_CustomerDietaryRestriction_Customer_CustomerEmail1",
                        column: x => x.CustomerEmail1,
                        principalTable: "Customer",
                        principalColumn: "Email",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerDietaryRestriction_CustomerEmail1",
                table: "CustomerDietaryRestriction",
                column: "CustomerEmail1");

            migrationBuilder.AddForeignKey(
                name: "FK_WeekOrder_Customer_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail",
                principalTable: "Customer",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeekOrder_Customer_CustomerEmail",
                table: "WeekOrder");

            migrationBuilder.DropTable(
                name: "CustomerDietaryRestriction");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder");

            migrationBuilder.AlterColumn<string>(
                name: "CustomerEmail",
                table: "WeekOrder",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
