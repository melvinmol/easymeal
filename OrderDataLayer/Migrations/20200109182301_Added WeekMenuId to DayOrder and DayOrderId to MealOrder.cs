﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class AddedWeekMenuIdtoDayOrderandDayOrderIdtoMealOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DayOrder_WeekOrder_WeekOrderId",
                table: "DayOrder");

            migrationBuilder.AddColumn<int>(
                name: "DayOrderId",
                table: "WeekOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "WeekOrderId",
                table: "DayOrder",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DayOrder_WeekOrder_WeekOrderId",
                table: "DayOrder",
                column: "WeekOrderId",
                principalTable: "WeekOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DayOrder_WeekOrder_WeekOrderId",
                table: "DayOrder");

            migrationBuilder.DropColumn(
                name: "DayOrderId",
                table: "WeekOrder");

            migrationBuilder.AlterColumn<int>(
                name: "WeekOrderId",
                table: "DayOrder",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_DayOrder_WeekOrder_WeekOrderId",
                table: "DayOrder",
                column: "WeekOrderId",
                principalTable: "WeekOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
