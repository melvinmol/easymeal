﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class Removedcustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeekOrder_Chef_CherEmail",
                table: "WeekOrder");

            migrationBuilder.RenameColumn(
                name: "CherEmail",
                table: "WeekOrder",
                newName: "ChefEmail");

            migrationBuilder.RenameIndex(
                name: "IX_WeekOrder_CherEmail",
                table: "WeekOrder",
                newName: "IX_WeekOrder_ChefEmail");

            migrationBuilder.AddForeignKey(
                name: "FK_WeekOrder_Chef_ChefEmail",
                table: "WeekOrder",
                column: "ChefEmail",
                principalTable: "Chef",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeekOrder_Chef_ChefEmail",
                table: "WeekOrder");

            migrationBuilder.RenameColumn(
                name: "ChefEmail",
                table: "WeekOrder",
                newName: "CherEmail");

            migrationBuilder.RenameIndex(
                name: "IX_WeekOrder_ChefEmail",
                table: "WeekOrder",
                newName: "IX_WeekOrder_CherEmail");

            migrationBuilder.AddForeignKey(
                name: "FK_WeekOrder_Chef_CherEmail",
                table: "WeekOrder",
                column: "CherEmail",
                principalTable: "Chef",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
