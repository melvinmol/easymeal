﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class AddedMealOnlyIdtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeekOrder_Chef_ChefEmail",
                table: "WeekOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_WeekOrder_Customer_CustomerEmail",
                table: "WeekOrder");

            migrationBuilder.DropTable(
                name: "Chef");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Meal");

            migrationBuilder.DropTable(
                name: "MealType");

            migrationBuilder.DropIndex(
                name: "IX_WeekOrder_ChefEmail",
                table: "WeekOrder");

            migrationBuilder.DropIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder");

            migrationBuilder.DropColumn(
                name: "ChefEmail",
                table: "WeekOrder");

            migrationBuilder.AlterColumn<string>(
                name: "CustomerEmail",
                table: "WeekOrder",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "MealOnlyId",
                columns: table => new
                {
                    MealId = table.Column<int>(nullable: false),
                    DayOrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealOnlyId", x => new { x.DayOrderId, x.MealId });
                    table.ForeignKey(
                        name: "FK_MealOnlyId_DayOrder_DayOrderId",
                        column: x => x.DayOrderId,
                        principalTable: "DayOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealOnlyId");

            migrationBuilder.AlterColumn<string>(
                name: "CustomerEmail",
                table: "WeekOrder",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChefEmail",
                table: "WeekOrder",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Chef",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chef", x => x.Email);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    Birthday = table.Column<DateTime>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    CustomerNumber = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    HouseNumber = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Email);
                });

            migrationBuilder.CreateTable(
                name: "MealType",
                columns: table => new
                {
                    TypeName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealType", x => x.TypeName);
                });

            migrationBuilder.CreateTable(
                name: "Meal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DayOrderId = table.Column<int>(nullable: true),
                    MealTypeTypeName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Meal_DayOrder_DayOrderId",
                        column: x => x.DayOrderId,
                        principalTable: "DayOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Meal_MealType_MealTypeTypeName",
                        column: x => x.MealTypeTypeName,
                        principalTable: "MealType",
                        principalColumn: "TypeName",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WeekOrder_ChefEmail",
                table: "WeekOrder",
                column: "ChefEmail");

            migrationBuilder.CreateIndex(
                name: "IX_WeekOrder_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail");

            migrationBuilder.CreateIndex(
                name: "IX_Meal_DayOrderId",
                table: "Meal",
                column: "DayOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Meal_MealTypeTypeName",
                table: "Meal",
                column: "MealTypeTypeName");

            migrationBuilder.AddForeignKey(
                name: "FK_WeekOrder_Chef_ChefEmail",
                table: "WeekOrder",
                column: "ChefEmail",
                principalTable: "Chef",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WeekOrder_Customer_CustomerEmail",
                table: "WeekOrder",
                column: "CustomerEmail",
                principalTable: "Customer",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
