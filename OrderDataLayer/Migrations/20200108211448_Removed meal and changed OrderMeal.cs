﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class RemovedmealandchangedOrderMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealOrder_Meal_MealId",
                table: "MealOrder");

            migrationBuilder.DropTable(
                name: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_MealOrder_MealId",
                table: "MealOrder");

            migrationBuilder.AlterColumn<int>(
                name: "MealId",
                table: "MealOrder",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "MealOrder",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MealType",
                table: "MealOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "MealOrder",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "MealOrder",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "MealOrder");

            migrationBuilder.DropColumn(
                name: "MealType",
                table: "MealOrder");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "MealOrder");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "MealOrder");

            migrationBuilder.AlterColumn<int>(
                name: "MealId",
                table: "MealOrder",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "Meal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Image = table.Column<byte[]>(nullable: true),
                    MealType = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Meal", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealOrder_MealId",
                table: "MealOrder",
                column: "MealId");

            migrationBuilder.AddForeignKey(
                name: "FK_MealOrder_Meal_MealId",
                table: "MealOrder",
                column: "MealId",
                principalTable: "Meal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
