﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class AddedMealOrdertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealOnlyId");

            migrationBuilder.CreateTable(
                name: "MealOrder",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Size = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    DayOrderId = table.Column<int>(nullable: true),
                    MealId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealOrder", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MealOrder_DayOrder_DayOrderId",
                        column: x => x.DayOrderId,
                        principalTable: "DayOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealOrder_DayOrderId",
                table: "MealOrder",
                column: "DayOrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealOrder");

            migrationBuilder.CreateTable(
                name: "MealOnlyId",
                columns: table => new
                {
                    DayOrderId = table.Column<int>(nullable: false),
                    MealId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealOnlyId", x => new { x.DayOrderId, x.MealId });
                    table.ForeignKey(
                        name: "FK_MealOnlyId_DayOrder_DayOrderId",
                        column: x => x.DayOrderId,
                        principalTable: "DayOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }
    }
}
