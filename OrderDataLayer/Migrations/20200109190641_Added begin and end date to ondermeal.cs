﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderDataLayer.Migrations
{
    public partial class Addedbeginandenddatetoondermeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BeginDate",
                table: "WeekOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "WeekOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BeginDate",
                table: "WeekOrder");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "WeekOrder");
        }
    }
}
