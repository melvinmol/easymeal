﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;

namespace OrderDataLayer.Repositories
{
    public class APIWeekMenuDAO : IWeekMenuDAO
    {
        private readonly string _baseUrlMenus = "https://apieasymealmelvin.azurewebsites.net/api/menus";

        public Task Create(WeekMenu weekMenu)
        {
            throw new NotImplementedException();
        }

        /*
         * Reads all WeekMenus
         * Return: all Meals
         */
        public async Task<List<WeekMenu>> Read()
        {
            return await RequestAPI<List<WeekMenu>>.Execute(_baseUrlMenus);
        }

        /*
         * Read WeekMenu by id
         * Return: WeekMenu by id
         */
        public async Task<WeekMenu> ReadById(int id)
        {
            return await RequestAPI<WeekMenu>.Execute($"{_baseUrlMenus}/{id}");
        }

        public Task Update(WeekMenu weekMenu)
        {
            throw new NotImplementedException();
        }

        public Task Delete(int weekMenu)
        {
            throw new NotImplementedException();
        }

        public Task<List<WeekMenu>> ReadWithoutMeals()
        {
            throw new NotImplementedException();
        }
    }
}
