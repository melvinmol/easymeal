﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.EntityFrameworkCore;

namespace OrderDataLayer.Repositories
{
    public class SQLWeekOrderDAO : IWeekOrderDAO
    {
        private OrderDbContext _dbContext;

        public SQLWeekOrderDAO(OrderDbContext orderDbContext)
        {
            this._dbContext = orderDbContext;
        }

        public async Task Create(WeekOrder weekOrder)
        {
            await _dbContext.WeekOrder.AddAsync(weekOrder);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<WeekOrder>> Read()
        {
            List<WeekOrder> weekOrders = await _dbContext.WeekOrder.ToListAsync();

            return weekOrders;
        }

        public async Task<WeekOrder> ReadById(int id)
        {
            return await _dbContext.WeekOrder.FirstOrDefaultAsync<WeekOrder>(weekOrder => weekOrder.Id == id);
        }

        public async Task<List<WeekOrder>> ReadByCustomerEmail(string email)
        {
            List<WeekOrder> weekOrders = await _dbContext
                .WeekOrder
                .Where<WeekOrder>(weekOrder => 
                weekOrder
                .Customer
                .Email
                .Contains(email))
                .ToListAsync();

            foreach(WeekOrder weekOrderSearch in weekOrders)
            {
                List<DayOrder> dayOrders = _dbContext.DayOrder
                    .Where<DayOrder>(dayOrder => 
                    dayOrder.WeekOrderId == weekOrderSearch.Id)
                    .ToList<DayOrder>();

                foreach(DayOrder dayOrderSearch in dayOrders)
                {
                    List<MealOrder> mealOrders = _dbContext.MealOrder
                        .Where<MealOrder>(mealOrder =>
                        mealOrder.DayOrderId == dayOrderSearch.Id)
                        .ToList<MealOrder>();

                    dayOrderSearch.MealOrders = mealOrders;
                }

                weekOrderSearch.DayOrders = dayOrders;
            }
            return weekOrders;
        }

        public async Task Update(WeekOrder newWeekOrder)
        {
            WeekOrder olderWeekOrder = await ReadById(newWeekOrder.Id);
            if (olderWeekOrder != null)
            {
                olderWeekOrder = newWeekOrder;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("WeekOrder is not found in the database");
            }
        }

        public async Task Delete(int id)
        {
            _dbContext.WeekOrder.Remove(await ReadById(id));
            await _dbContext.SaveChangesAsync();
        }
    }
}
