﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;

namespace OrderDataLayer.Repositories
{
    public class APIMealDAO : IMealDAO
    {
        private readonly string _baseUrlMeals = "https://apieasymealmelvin.azurewebsites.net/api/meals";
        private readonly string _baseUrlMenus = "https://apieasymealmelvin.azurewebsites.net/api/menus";


        public Task Create(Meal meal)
        {
            throw new NotImplementedException("It is not possible to create a Meal");
        }

        /*
         * Reads all meals
         * Return: all meals
         */
        public async Task<List<Meal>> Read()
        {
            return await RequestAPI<List<Meal>>.Execute(_baseUrlMeals);
        }

        /*
         * Read meal by id
         * Return: meal by id
         */
        public async Task<Meal> ReadById(int id)
        {
            return await RequestAPI<Meal>.Execute($"{_baseUrlMeals}/{id}");
        }

        public async Task<List<Meal>> ReadByWeekMenuId(int id)
        {
            return await RequestAPI<List<Meal>>.Execute($"{_baseUrlMenus}/{id}/meals");
        }

        public Task Update(Meal meal)
        {
            throw new NotImplementedException("It is not possible to update a Meal");
        }

        public Task Delete(int meal)
        {
            throw new NotImplementedException("It is not possible to delete a Meal");
        }
    }
}
