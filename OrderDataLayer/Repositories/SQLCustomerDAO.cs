﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using OrderDataLayer.Models;
using Microsoft.EntityFrameworkCore;
using OrderDataLayer.Tables;

namespace OrderDataLayer.Repositories
{
    public class SQLCustomerDAO : ICustomerDAO
    {
        private OrderDbContext _dbContext;

        public SQLCustomerDAO(OrderDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /**
         * Create Customer in database
         * Add DietaryRestrictions to Customer
         * Create IdentityUser in database
         * Param: new customer, new IdentityUser
         */
        public async Task Create(Customer customer)
        {
            await _dbContext.Customer.AddAsync(customer);
            foreach (var dietaryRestriction in customer.DietaryRestrictions
                .Where(dietaryRestriction => dietaryRestriction != null))
            {
                await _dbContext.AddAsync(new CustomerDietaryRestriction()
                {
                    DietaryRestrictionName = dietaryRestriction.Name,
                    CustomerEmail = customer.Email
                });
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Customer>> Read()
        {
            return await _dbContext.Customer.ToListAsync();
        }

        /**
         * Read Customer by email
         * Param: email to find Customer
         */
        public async Task<Customer> ReadByEmail(string email)
        {
            var founded = await _dbContext.Customer.FirstOrDefaultAsync<Customer>(customer => customer.Email.Equals(email));
            await AddDietaryRestrictionToCustomer(founded);
            return founded;
        }

        /**
         * Find Customer by email
         * Update Customer
         * Param: new Customer 
         */
        public async Task Update(Customer newCustomer)
        {
            _dbContext.Customer.Update(newCustomer);
            await _dbContext.SaveChangesAsync();
        }

        /**
         * Find Customer by email
         * Delete Customer by email
         * Param: Customer's email 
         */
        public async Task Delete(string email)
        {
            var foundedCustomer = await ReadByEmail(email);

            var customerDietaryRestrictions = _dbContext.CustomerDietaryRestriction.Where<CustomerDietaryRestriction>(
                customer => foundedCustomer.Email.Equals(customer.CustomerEmail));

            _dbContext.Customer.Remove(await ReadByEmail(email));

            _dbContext.CustomerDietaryRestriction
                .RemoveRange(customerDietaryRestrictions);

            await _dbContext.SaveChangesAsync();
        }

        /*
         * Add DietaryRestriction to Customer
         * Param: Customer
         */
        private async Task AddDietaryRestrictionToCustomer(Customer customer)
        {
            List<CustomerDietaryRestriction> dietaryRestrictions = await (from dietaryRestriction in _dbContext.CustomerDietaryRestriction
                                                                          where dietaryRestriction.CustomerEmail.Equals(customer.Email)
                select dietaryRestriction).ToListAsync();
            AddMealDietaryRestrictionInCustomer.Execute(customer, dietaryRestrictions);
        }
    }
}
