﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.EntityFrameworkCore;

namespace OrderDataLayer.Repositories
{
    public class SQLDayOrderDAO : IDayOrderDAO
    {
        private OrderDbContext _dbContext;

        public SQLDayOrderDAO(OrderDbContext orderDbContext)
        {
            this._dbContext = orderDbContext;
        }

        public async Task Create(DayOrder dayOrder)
        {
            await _dbContext.DayOrder.AddAsync(dayOrder);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<DayOrder>> Read()
        {
            return await _dbContext.DayOrder.ToListAsync();
        }

        public async Task<DayOrder> ReadById(int id)
        {
            return await _dbContext.DayOrder.FirstOrDefaultAsync<DayOrder>(dayOrder => dayOrder.Id == id);
        }

        public async Task Update(DayOrder newDayOrder)
        {
            DayOrder oldDayOrder = await ReadById(newDayOrder.Id);
            if (oldDayOrder != null)
            {
                oldDayOrder = newDayOrder;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Meal is not found in the database");
            }
        }

        public async Task Delete(int id)
        {
            _dbContext.DayOrder.Remove(await ReadById(id));
            await _dbContext.SaveChangesAsync();
        }
    }
}
