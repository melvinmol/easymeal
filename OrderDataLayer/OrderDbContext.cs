﻿using DomainModel;
using Microsoft.EntityFrameworkCore;
using OrderDataLayer.Tables;

namespace OrderDataLayer
{
    public class OrderDbContext : DbContext
    {
        public OrderDbContext(DbContextOptions<OrderDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CustomerDietaryRestriction>().HasKey(customerDietaryRestriction => new {
                customerDietaryRestriction.CustomerEmail,
                customerDietaryRestriction.DietaryRestrictionName
            });

            modelBuilder.Entity<Customer>().Ignore(customer => customer.DietaryRestrictions);
            modelBuilder.Entity<Customer>().HasKey(customer => customer.Email);
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<CustomerDietaryRestriction> CustomerDietaryRestriction { get; set; }
        public DbSet<MealOrder> MealOrder { get; set; }
        public DbSet<WeekOrder> WeekOrder { get; set; }
        public DbSet<DayOrder> DayOrder { get; set; }
    }
}
