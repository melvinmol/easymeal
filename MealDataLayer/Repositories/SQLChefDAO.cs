﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.EntityFrameworkCore;

namespace MealDataLayer.Repositories
{
    public class SQLChefDAO : IChefDAO
    {
        private MealDbContext _dbContext;

        public SQLChefDAO(MealDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /**
         * Create Chef in database
         * Create IdentityUser in database
         * Param: new chef, new IdentityUser
         */
        public async Task Create(Chef chef)
        {
            await _dbContext.Chef.AddAsync(chef);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Chef>> Read()
        {
            return await _dbContext.Chef.ToListAsync();
        }

        /**
         * Read Chef by email
         * Param: email to find Chef
         */
        public async Task<Chef> ReadByEmail(string email)
        {
            var founded = await _dbContext.Chef.FirstOrDefaultAsync<Chef>(chef => chef.Email.Equals(email));
            return founded;
        }

        /**
         * Find Chef by email
         * Update Chef
         * Param: new Chef 
         */
        public async Task Update(Chef newChef)
        {
            await Delete(newChef.Email);
            await Create(newChef);
            await _dbContext.SaveChangesAsync();
        }

        /**
         * Delete Chef by email
         * Param: Chef's email 
         */
        public async Task Delete(string email)
        {
            _dbContext.Chef.Remove(await ReadByEmail(email));

            await _dbContext.SaveChangesAsync();
        }
    }
}
