﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using MealDataLayer.Tables;
using Microsoft.EntityFrameworkCore;

namespace MealDataLayer.Repositories
{
    public class SQLWeekMenuDAO : IWeekMenuDAO
    {
        private MealDbContext _dbContext;

        public SQLWeekMenuDAO(MealDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(WeekMenu weekMenu)
        {
            await _dbContext.WeekMenu.AddAsync(weekMenu);
            foreach(Meal meal in weekMenu.Meals)
            {
                await _dbContext.MealWeekMenu.AddAsync(new MealWeekMenu
                {
                    MealId = meal.Id,
                    WeekMenuId = weekMenu.Id
                });
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<WeekMenu>> Read()
        {
            List<WeekMenu> weekMenus = await _dbContext.WeekMenu.ToListAsync();
            foreach (WeekMenu weekMenu in weekMenus)
            {
                IList<MealWeekMenu> mealWeekMenus = await _dbContext.MealWeekMenu
                    .Where<MealWeekMenu>(mealWeekMenu => mealWeekMenu.WeekMenuId == weekMenu.Id).ToListAsync();
                IList<Meal> meals = new List<Meal>();
                foreach (MealWeekMenu mealWeekMenu in mealWeekMenus)
                {
                    meals.Add(new SQLMealDAO(_dbContext).ReadById(mealWeekMenu.MealId).Result);
                }
                weekMenu.Meals = meals;
            }
            return weekMenus;
        }

        public async Task<List<WeekMenu>> ReadWithoutMeals()
        {
            List<WeekMenu> weekMenus = await _dbContext.WeekMenu.ToListAsync();
            return weekMenus;
        }

        public async Task<WeekMenu> ReadById(int id)
        {
            IList<MealWeekMenu> mealWeekMenus = await _dbContext.MealWeekMenu
                .Where<MealWeekMenu>(mealWeekMenu => mealWeekMenu.WeekMenuId == id).ToListAsync();
            WeekMenu weekMenu = await _dbContext.WeekMenu.FirstOrDefaultAsync<WeekMenu>(w => w.Id == id);
            IList<Meal> meals = new List<Meal>();
            foreach (MealWeekMenu mealWeekMenu in mealWeekMenus)
            {
                meals.Add(new SQLMealDAO(_dbContext).ReadById(mealWeekMenu.MealId).Result);
            }

            weekMenu.Meals = meals;
            return weekMenu;
        }

        public async Task Update(WeekMenu newWeekMenu)
        {
            WeekMenu oldWeekMenu = await ReadById(newWeekMenu.Id);
            if (oldWeekMenu != null)
            {
                oldWeekMenu = newWeekMenu;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Meal is not found in the database");
            }
        }

        public async Task Delete(int id)
        {
            _dbContext.WeekMenu.Remove(await ReadById(id));
            await _dbContext.SaveChangesAsync();
        }
    }
}
