﻿using DomainModel;
using DomainServices.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using MealDataLayer.Models;
using MealDataLayer.Tables;
using Microsoft.EntityFrameworkCore;

namespace MealDataLayer.Repositories
{
    public class SQLMealDAO : IMealDAO
    {
        private MealDbContext _dbContext;

        public SQLMealDAO(MealDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(Meal meal)
        {
            await _dbContext.Meal.AddAsync(meal);
            foreach (DietaryRestriction diet in meal.Diets)
            {
                if (diet != null)
                {
                    await _dbContext.AddAsync<MealDietaryRestriction>(new MealDietaryRestriction()
                    {
                        DietaryRestrictionName = diet.Name,
                        MealId = meal.Id
                    });
                }
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Meal>> Read()
        {
            List<Meal> meals = await _dbContext.Meal.ToListAsync();
            foreach (Meal meal in meals)
            {
                await AddDietaryRestrictionToMeal(meal);
            }
            return meals;
        }

        public async Task<List<Meal>> ReadByWeekMenuId(int id)
        {
            List<MealWeekMenu> mealWeekMenus = await _dbContext.MealWeekMenu.Where<MealWeekMenu>(mealWeekMenu =>
                mealWeekMenu.WeekMenuId == id).ToListAsync();
            List<Meal> meals = new List<Meal>();
            foreach (MealWeekMenu mealWeekMenu in mealWeekMenus)
            {
                meals.Add(await _dbContext.Meal.FirstOrDefaultAsync<Meal>(meal => meal.Id == mealWeekMenu.MealId));
            }
            foreach (Meal meal in meals)
            {
                await AddDietaryRestrictionToMeal(meal);
            }
            return meals;
        }

        public async Task<Meal> ReadById(int id)
        {
            Meal foundMeal = await _dbContext.Meal.FirstOrDefaultAsync<Meal>(meal => meal.Id == id);
            await AddDietaryRestrictionToMeal(foundMeal);
            return foundMeal;
        }

        public async Task Update(Meal newMeal)
        {
            Meal oldMeal = await ReadById(newMeal.Id);
            if (oldMeal != null)
            {
                oldMeal = newMeal;
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new ArgumentException("Meal is not found in the database");
            }
        }

        public async Task Delete(int id)
        {
            _dbContext.Meal.Remove(await ReadById(id));
            await _dbContext.SaveChangesAsync();
        }

        private async Task AddDietaryRestrictionToMeal(Meal meal)
        {
            List<MealDietaryRestriction> dietaryRestrictions = await (from dietaryRestriction in _dbContext.MealDietaryRestriction
                where dietaryRestriction.MealId.Equals(meal.Id)
                select dietaryRestriction).ToListAsync();
            AddMealDietaryRestrictionInMeal.Execute(meal, dietaryRestrictions);
        }
    }
}
