﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;
using MealDataLayer.Tables;

namespace MealDataLayer.Models
{
    public static class AddMealDietaryRestrictionInMeal
    {
        public static void Execute(Meal meal, IList<MealDietaryRestriction> mealDietaryRestrictions)
        {
            IList<DietaryRestriction> dietaryRestrictions = new List<DietaryRestriction>();
            foreach (MealDietaryRestriction mealRestriction in mealDietaryRestrictions)
            {
                dietaryRestrictions.Add(new DietaryRestriction()
                {
                    Name = mealRestriction.DietaryRestrictionName
                });
            }
            meal.Diets = dietaryRestrictions;
        }
    }
}
