﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MealDataLayer.Tables
{
    public class MealDietaryRestriction
    {
        public int MealId { get; set; }
        public Meal Meal { get; set; }

        public string DietaryRestrictionName { get; set; }
    }
}
