﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;

namespace MealDataLayer.Tables
{
    public class MealWeekMenu
    {
        public int MealId { get; set; }
        public Meal Meal { get; set; }

        public int WeekMenuId { get; set; }
        public WeekMenu WeekMenu { get; set; }
    }
}
