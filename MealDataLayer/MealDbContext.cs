﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;
using MealDataLayer.Tables;

namespace MealDataLayer
{
    public class MealDbContext : DbContext
    {
        public MealDbContext(DbContextOptions<MealDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // MealDietaryRestriction linked table
            modelBuilder.Entity<MealDietaryRestriction>().HasKey(mealDietaryRestriction => new {
                mealDietaryRestriction.MealId,
                mealDietaryRestriction.DietaryRestrictionName
            });

            // MealWeekMenu linked table
            modelBuilder.Entity<MealWeekMenu>().HasKey(mealWeekMenu => new {
                mealWeekMenu.MealId,
                mealWeekMenu.WeekMenuId
            });

            modelBuilder.Entity<Meal>().Ignore(meal => meal.Diets);
            modelBuilder.Entity<WeekMenu>().Ignore(weekMenu => weekMenu.Meals);

            modelBuilder.Entity<Meal>().Property(meal => meal.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Chef>().HasKey(chef => chef.Email);
        }
        public DbSet<Chef> Chef { get; set; }
        public DbSet<WeekMenu> WeekMenu { get; set; }
        public DbSet<MealDietaryRestriction> MealDietaryRestriction { get; set; }
        public DbSet<MealWeekMenu> MealWeekMenu { get; set; }
        public DbSet<Meal> Meal { get; set; }
    }
}
