﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class ChangedMealTypetoenum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_MealTypeTypeName",
                table: "Meal");

            migrationBuilder.DropTable(
                name: "MealType");

            migrationBuilder.DropIndex(
                name: "IX_Meal_MealTypeTypeName",
                table: "Meal");

            migrationBuilder.DropColumn(
                name: "MealTypeTypeName",
                table: "Meal");

            migrationBuilder.AddColumn<int>(
                name: "MealType",
                table: "Meal",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MealType",
                table: "Meal");

            migrationBuilder.AddColumn<string>(
                name: "MealTypeTypeName",
                table: "Meal",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MealType",
                columns: table => new
                {
                    TypeName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealType", x => x.TypeName);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Meal_MealTypeTypeName",
                table: "Meal",
                column: "MealTypeTypeName");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_MealTypeTypeName",
                table: "Meal",
                column: "MealTypeTypeName",
                principalTable: "MealType",
                principalColumn: "TypeName",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
