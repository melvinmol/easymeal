﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class AddedMealWeekMenulinkedmenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_WeekMenu_WeekMenuId",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_WeekMenuId",
                table: "Meal");

            migrationBuilder.DropColumn(
                name: "WeekMenuId",
                table: "Meal");

            migrationBuilder.CreateTable(
                name: "MealWeekMenu",
                columns: table => new
                {
                    MealId = table.Column<int>(nullable: false),
                    WeekMenuId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealWeekMenu", x => new { x.MealId, x.WeekMenuId });
                    table.ForeignKey(
                        name: "FK_MealWeekMenu_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MealWeekMenu_WeekMenu_WeekMenuId",
                        column: x => x.WeekMenuId,
                        principalTable: "WeekMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealWeekMenu_WeekMenuId",
                table: "MealWeekMenu",
                column: "WeekMenuId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealWeekMenu");

            migrationBuilder.AddColumn<int>(
                name: "WeekMenuId",
                table: "Meal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_WeekMenuId",
                table: "Meal",
                column: "WeekMenuId");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_WeekMenu_WeekMenuId",
                table: "Meal",
                column: "WeekMenuId",
                principalTable: "WeekMenu",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
