﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class ChangedintableMealTypenametoTypeName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_Name",
                table: "Meal");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "MealType",
                newName: "TypeName");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MealTypeTypeName",
                table: "Meal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_MealTypeTypeName",
                table: "Meal",
                column: "MealTypeTypeName");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_MealTypeTypeName",
                table: "Meal",
                column: "MealTypeTypeName",
                principalTable: "MealType",
                principalColumn: "TypeName",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_MealTypeTypeName",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_MealTypeTypeName",
                table: "Meal");

            migrationBuilder.DropColumn(
                name: "MealTypeTypeName",
                table: "Meal");

            migrationBuilder.RenameColumn(
                name: "TypeName",
                table: "MealType",
                newName: "Name");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_Name",
                table: "Meal",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal",
                column: "Name",
                principalTable: "MealType",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
