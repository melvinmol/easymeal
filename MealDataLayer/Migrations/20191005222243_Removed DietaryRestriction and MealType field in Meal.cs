﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class RemovedDietaryRestrictionandMealTypefieldinMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DietaryRestriction_Meal_MealId",
                table: "DietaryRestriction");

            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_TypeId",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_TypeId",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_DietaryRestriction_MealId",
                table: "DietaryRestriction");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "Meal");

            migrationBuilder.DropColumn(
                name: "MealId",
                table: "DietaryRestriction");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TypeId",
                table: "Meal",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MealId",
                table: "DietaryRestriction",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_TypeId",
                table: "Meal",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DietaryRestriction_MealId",
                table: "DietaryRestriction",
                column: "MealId");

            migrationBuilder.AddForeignKey(
                name: "FK_DietaryRestriction_Meal_MealId",
                table: "DietaryRestriction",
                column: "MealId",
                principalTable: "Meal",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_TypeId",
                table: "Meal",
                column: "TypeId",
                principalTable: "MealType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
