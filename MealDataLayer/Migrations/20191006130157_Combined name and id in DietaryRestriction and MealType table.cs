﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class CombinednameandidinDietaryRestrictionandMealTypetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionId",
                table: "MealDietaryRestriction");

            migrationBuilder.DropForeignKey(
                name: "FK_MealMealType_MealType_MealTypeId",
                table: "MealMealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealType",
                table: "MealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealMealType",
                table: "MealMealType");

            migrationBuilder.DropIndex(
                name: "IX_MealMealType_MealTypeId",
                table: "MealMealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealDietaryRestriction",
                table: "MealDietaryRestriction");

            migrationBuilder.DropIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionId",
                table: "MealDietaryRestriction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DietaryRestriction",
                table: "DietaryRestriction");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "MealType");

            migrationBuilder.DropColumn(
                name: "MealTypeId",
                table: "MealMealType");

            migrationBuilder.DropColumn(
                name: "DietaryRestrictionId",
                table: "MealDietaryRestriction");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "DietaryRestriction");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MealType",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MealTypeName",
                table: "MealMealType",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DietaryRestrictionName",
                table: "MealDietaryRestriction",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DietaryRestriction",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealType",
                table: "MealType",
                column: "Name");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealMealType",
                table: "MealMealType",
                columns: new[] { "MealId", "MealTypeName" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealDietaryRestriction",
                table: "MealDietaryRestriction",
                columns: new[] { "MealId", "DietaryRestrictionName" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_DietaryRestriction",
                table: "DietaryRestriction",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_MealMealType_MealTypeName",
                table: "MealMealType",
                column: "MealTypeName");

            migrationBuilder.CreateIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionName");

            migrationBuilder.AddForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionName",
                principalTable: "DietaryRestriction",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MealMealType_MealType_MealTypeName",
                table: "MealMealType",
                column: "MealTypeName",
                principalTable: "MealType",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction");

            migrationBuilder.DropForeignKey(
                name: "FK_MealMealType_MealType_MealTypeName",
                table: "MealMealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealType",
                table: "MealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealMealType",
                table: "MealMealType");

            migrationBuilder.DropIndex(
                name: "IX_MealMealType_MealTypeName",
                table: "MealMealType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MealDietaryRestriction",
                table: "MealDietaryRestriction");

            migrationBuilder.DropIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DietaryRestriction",
                table: "DietaryRestriction");

            migrationBuilder.DropColumn(
                name: "MealTypeName",
                table: "MealMealType");

            migrationBuilder.DropColumn(
                name: "DietaryRestrictionName",
                table: "MealDietaryRestriction");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "MealType",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "MealType",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<int>(
                name: "MealTypeId",
                table: "MealMealType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DietaryRestrictionId",
                table: "MealDietaryRestriction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "DietaryRestriction",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "DietaryRestriction",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealType",
                table: "MealType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealMealType",
                table: "MealMealType",
                columns: new[] { "MealId", "MealTypeId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_MealDietaryRestriction",
                table: "MealDietaryRestriction",
                columns: new[] { "MealId", "DietaryRestrictionId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_DietaryRestriction",
                table: "DietaryRestriction",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MealMealType_MealTypeId",
                table: "MealMealType",
                column: "MealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionId",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionId");

            migrationBuilder.AddForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionId",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionId",
                principalTable: "DietaryRestriction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MealMealType_MealType_MealTypeId",
                table: "MealMealType",
                column: "MealTypeId",
                principalTable: "MealType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
