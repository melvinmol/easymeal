﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class AddedlindedtablesMealDietaryRestrictionandMealMealType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MealDietaryRestriction",
                columns: table => new
                {
                    MealId = table.Column<int>(nullable: false),
                    DietaryRestrictionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealDietaryRestriction", x => new { x.MealId, x.DietaryRestrictionId });
                    table.ForeignKey(
                        name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionId",
                        column: x => x.DietaryRestrictionId,
                        principalTable: "DietaryRestriction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MealDietaryRestriction_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MealMealType",
                columns: table => new
                {
                    MealId = table.Column<int>(nullable: false),
                    MealTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealMealType", x => new { x.MealId, x.MealTypeId });
                    table.ForeignKey(
                        name: "FK_MealMealType_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MealMealType_MealType_MealTypeId",
                        column: x => x.MealTypeId,
                        principalTable: "MealType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionId",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionId");

            migrationBuilder.CreateIndex(
                name: "IX_MealMealType_MealTypeId",
                table: "MealMealType",
                column: "MealTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealDietaryRestriction");

            migrationBuilder.DropTable(
                name: "MealMealType");
        }
    }
}
