﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class RemovedDietaryRestrictiontable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction");

            migrationBuilder.DropTable(
                name: "DietaryRestriction");

            migrationBuilder.DropIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DietaryRestriction",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DietaryRestriction", x => x.Name);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealDietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionName");

            migrationBuilder.AddForeignKey(
                name: "FK_MealDietaryRestriction_DietaryRestriction_DietaryRestrictionName",
                table: "MealDietaryRestriction",
                column: "DietaryRestrictionName",
                principalTable: "DietaryRestriction",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
