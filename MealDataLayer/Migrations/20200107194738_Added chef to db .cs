﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class Addedcheftodb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ChefEmail",
                table: "WeekMenu",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Price",
                table: "Meal",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.CreateTable(
                name: "Chef",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chef", x => x.Email);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WeekMenu_ChefEmail",
                table: "WeekMenu",
                column: "ChefEmail");

            migrationBuilder.AddForeignKey(
                name: "FK_WeekMenu_Chef_ChefEmail",
                table: "WeekMenu",
                column: "ChefEmail",
                principalTable: "Chef",
                principalColumn: "Email",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeekMenu_Chef_ChefEmail",
                table: "WeekMenu");

            migrationBuilder.DropTable(
                name: "Chef");

            migrationBuilder.DropIndex(
                name: "IX_WeekMenu_ChefEmail",
                table: "WeekMenu");

            migrationBuilder.AlterColumn<string>(
                name: "ChefEmail",
                table: "WeekMenu",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Meal",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
