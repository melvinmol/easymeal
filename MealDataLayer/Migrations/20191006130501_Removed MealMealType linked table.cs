﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class RemovedMealMealTypelinkedtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MealMealType");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_Name",
                table: "Meal",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal",
                column: "Name",
                principalTable: "MealType",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_Name",
                table: "Meal");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "MealMealType",
                columns: table => new
                {
                    MealId = table.Column<int>(nullable: false),
                    MealTypeName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MealMealType", x => new { x.MealId, x.MealTypeName });
                    table.ForeignKey(
                        name: "FK_MealMealType_Meal_MealId",
                        column: x => x.MealId,
                        principalTable: "Meal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MealMealType_MealType_MealTypeName",
                        column: x => x.MealTypeName,
                        principalTable: "MealType",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MealMealType_MealTypeName",
                table: "MealMealType",
                column: "MealTypeName");
        }
    }
}
