﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class ChangedintableMealnameTypetoMealType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_Type1",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_Type1",
                table: "Meal");

            migrationBuilder.DropColumn(
                name: "Type1",
                table: "Meal");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "MealType",
                newName: "Name");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_Name",
                table: "Meal",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal",
                column: "Name",
                principalTable: "MealType",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Meal_MealType_Name",
                table: "Meal");

            migrationBuilder.DropIndex(
                name: "IX_Meal_Name",
                table: "Meal");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "MealType",
                newName: "Type");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Meal",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type1",
                table: "Meal",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Meal_Type1",
                table: "Meal",
                column: "Type1");

            migrationBuilder.AddForeignKey(
                name: "FK_Meal_MealType_Type1",
                table: "Meal",
                column: "Type1",
                principalTable: "MealType",
                principalColumn: "Type",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
