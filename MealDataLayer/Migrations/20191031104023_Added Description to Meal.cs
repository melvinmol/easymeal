﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealDataLayer.Migrations
{
    public partial class AddedDescriptiontoMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Meal",
                nullable: false,
                oldClrType: typeof(float));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Meal",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Meal");

            migrationBuilder.AlterColumn<float>(
                name: "Price",
                table: "Meal",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
