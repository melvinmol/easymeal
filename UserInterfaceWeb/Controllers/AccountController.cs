﻿using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserInterfaceWeb.Models;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Controllers
{
    public class AccountController : Controller
    {
        private ICustomerDAO customerDAO;
        private UserManager<IdentityUser> userManager;
        private SignInManager<IdentityUser> signInManager;

        public AccountController(ICustomerDAO customerDAO, UserManager<IdentityUser> userMgr,
            SignInManager<IdentityUser> signInMgr)
        {
            this.customerDAO = customerDAO;
            userManager = userMgr;
            signInManager = signInMgr;
        }

        /*
         * Get SignUp page
         * Returns: SignUp page
         */
        [AllowAnonymous]
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }

        /*
         * Post Customer
         * Post IdentityUser
         * Check if IdentityUser if not return error
         * Param: new Customer
         * Returns: Order Index page
         */
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SignUp(SignUpViewModel signUpViewModel)
        {
            if (ModelState.IsValid)
            {
                if (await SignUpHelper.SignUp(signUpViewModel, userManager,
                    signInManager, customerDAO))
                {
                    return RedirectToAction("Index", "Order");
                }
                ModelState.AddModelError("SignUpFaild",
                    "Het aanmelden is niet gelukt.");
            }
            return View();
        }

        /*
         * Get Login page
         * Returns: Login page
         */
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        /*
         * Post email and password
         * Checks if ModelState is valid, if not return error
         * Checks if email and password match, if not return error
         * Param: ViewModel with email and password
         * Returns: Order Index page
         */
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                if (await LoginHelper.Login(loginViewModel, userManager, signInManager))
                {
                    return RedirectToAction("Index", "Order");
                }
                ModelState.AddModelError("EmailNotExitsOrPasswordIncorrect",
                    "Het emailadres bestaat niet, of het wachtwoord in incorrect");
            }
            return View(loginViewModel);
        }

        /*
         * Logout user
         * Returns: Home Index page
         */
        public async Task<RedirectResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Redirect("/");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EditProfile()
        {
            IdentityUser user = await userManager.GetUserAsync(HttpContext.User);
            SignUpViewModel signUpViewModel = await EditProfileHelper.IdentityUserToSignUpViewModel(user, customerDAO);
            return View("SignUp", signUpViewModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> EditProfile(SignUpViewModel signUpViewModel)
        {
            if (ModelState.IsValid)
            {
                await EditProfileHelper.UpdateCustomer(signUpViewModel, customerDAO);
                return RedirectToAction("Index", "Order");
            }
            return View("SignUp", signUpViewModel);
        }
    }
}
