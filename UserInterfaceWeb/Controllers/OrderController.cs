﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserInterfaceWeb.Models;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Controllers
{

    [Authorize]
    public class OrderController : Controller
    {
        private IWeekMenuDAO weekMenuDAO;
        private IWeekOrderDAO weekOrderDAO;
        private ICustomerDAO customerDAO;
        private DayOrderViewModelCart dayOrderViewModelCart;
        private UserManager<IdentityUser> userManager;

        public OrderController(
            IWeekMenuDAO weekMenuDAO,
            ICustomerDAO customerDAO,
            IWeekOrderDAO weekOrderDAO,
            DayOrderViewModelCart dayOrderViewModelCart,
            UserManager<IdentityUser> userMgr)
        {
            this.weekMenuDAO = weekMenuDAO;
            this.customerDAO = customerDAO;
            this.weekOrderDAO = weekOrderDAO;
            this.dayOrderViewModelCart = dayOrderViewModelCart;
            userManager = userMgr;
        }

        public async Task<IActionResult> Index()
        {
            IdentityUser user = await userManager.GetUserAsync(HttpContext.User);

            IList<WeekOrder> weekOrders = await weekOrderDAO.ReadByCustomerEmail(user.UserName);
            return View(weekOrders);
        }

        [HttpPost]
        public IActionResult Add(WeekMenu weekMenu)
        {
            dayOrderViewModelCart.Clear();
            return View(weekMenu);
        }

        [HttpPost]
        public async Task<ActionResult> AddWeekOrder(WeekMenu weekMenu)
        {
            IdentityUser user = await userManager.GetUserAsync(HttpContext.User);

            IList<string> validations =
                await AddWeekOrderHelper.AddWeekOrder(
                dayOrderViewModelCart,
                weekMenu,
                weekOrderDAO,
                customerDAO,
                user);

            dayOrderViewModelCart.Clear();

            if (validations.Count == 0)
            {
                return Redirect("Index");
            }

            foreach (string validation in validations)
            {
                ModelState.AddModelError("AddMealError", validation);
            }
            return View("Add", weekMenu);
        }

        public IActionResult Detail()
        {
            return View();
        }

        public IActionResult AddMealToOrder(DayOrderViewModel dayOrderViewModel)
        {
            dayOrderViewModelCart.AddItem(dayOrderViewModel, 1);
            return Redirect("Add");
        }


        public async Task<IActionResult> Invoice(MealMonthInvoice mealMonthInvoice)
        {
            IdentityUser user = await userManager.GetUserAsync(HttpContext.User);

            MealMonthInvoice found = await InvoiceHelper.FindMealMonthInvoice(weekOrderDAO, mealMonthInvoice, user, customerDAO);
            
            return View(found);
        }
    }
}
