﻿const week = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag']

$(document).ready(function () {
    hideCart()

    dayForward()
    dayBackward()

    addMealToOrderMinus20()
    addMealToOrderNormal()
    addMealToOrderPlus20()
})

function hideCart() {
    $(".shopping-cart-monday").hide()
    $(".shopping-cart-tuesday").hide()
    $(".shopping-cart-wednesday").hide()
    $(".shopping-cart-thursday").hide()
    $(".shopping-cart-friday").hide()
    $(".shopping-cart-saturday").hide()
    $(".shopping-cart-sunday").hide()
}

function dayForward() {
    $(".day-forward").click(function () {
        const posistion = week.indexOf($(this).parent().find(".input-name").val()) + 1
        if (posistion <= 6) {
            const newDay = week[posistion]
            $(this).removeClass('disabled')
            $(".day-backward").removeClass('disabled')
            if (posistion === 0) {
                $(".day-backward").addClass('disabled')
            } else if (posistion === 6) {
                $(this).addClass('disabled')
            }
            $(this).parent().find(".input-name").val(newDay)
            $(this).parent().find(".day-name").html(newDay)
        }
    })
}

function dayBackward() {
    $(".day-backward").click(function () {
        const posistion = week.indexOf($(this).parent().find(".input-name").val()) - 1
        if (posistion >= 0) {
            const newDay = week[posistion]
            $(this).removeClass('disabled')
            $(".day-forward").removeClass('disabled')
            if (posistion === 0) {
                $(".day-backward").addClass('disabled')
            } else if (posistion === 6) {
                $(this).addClass('disabled')
            }
           
            $(this).parent().find(".input-name").val(newDay)
            $(this).parent().find(".day-name").html(newDay)
        }
    })
}

function addMealToOrderMinus20() {
    $(".add-meal-to-order-minus-20").click(function () {
        addMealToOrder($(this), "Minus20")
    })
}

function addMealToOrderNormal() {
    $(".add-meal-to-order-normal").click(function () {
        addMealToOrder($(this), "Normal")
    })
}

function addMealToOrderPlus20() {
    $(".add-meal-to-order-plus-20").click(function () {
        addMealToOrder($(this), "Plus20")
    })
}

function addMealToOrder(object, size) {
    const day = translateDutchDayToEnglish($(".input-name").val())
    const id = object.parent().find(".meal-id").val()
    const type = object.parent().find(".meal-type").val()
    const name = object.parent().find(".meal-name").val()
    let price = object.parent().find(".meal-price").val()

    if (size === 'Plus20') {
        price = Math.round(price * 1.20)
    } else if (size === 'Minus20') {
        price = Math.round(price * 0.80)
    }

    const DayOrderViewModel = {
        Day: day,
        MealOrder: {
            MealId: id,
            MealType: type,
            Name: name,
            Price: price,
            Size: size
        }
    }

    $.ajax({
        type: "POST",
        url: "AddMealToOrder",
        data: DayOrderViewModel
    })
    $('#exampleModal' + id).modal('hide')
    addToShoppingCart(DayOrderViewModel)
}

function addToShoppingCart(DayOrderViewModel) {
    let dayObject = $(getShoppingCartClass(DayOrderViewModel.Day))

    let mealOrder = DayOrderViewModel.MealOrder

    let mealTypeObject = dayObject.find("." + mealOrder.MealType + "-meal")
    mealTypeObject.append(`<p title="${mealOrder.Size}"><b> ${translateEnglishMealTypeToDutch(mealOrder.MealType)}
                            :</b> ${mealOrder.Name}</p>`)

    dayObject.show()
    mealTypeObject.show()

    let totalPrice = parseInt($(".total-price").html())
    totalPrice += parseInt(mealOrder.Price)
    $(".total-price").html(totalPrice)
}

function translateEnglishMealTypeToDutch(mealType) {
    let mealTypeDutch = ""
    switch (mealType) {
        case "Starter":
            mealTypeDutch = "Voorgerecht"
            break;
        case "MainDish":
            mealTypeDutch = "Hoofdgerecht"
            break;
        case "Dessert":
            mealTypeDutch = "Nagerecht"
            break;
        default:
            break;
    }
    return mealTypeDutch
}

function getShoppingCartClass(day) {
    let cartClass = ""
    switch (day) {
        case "Monday":
            cartClass = ".shopping-cart-monday"
            break;
        case "Tuesday":
            cartClass = ".shopping-cart-tuesday"
            break;
        case "Wednesday":
            cartClass = ".shopping-cart-wednesday"
            break;
        case "Thursday":
            cartClass = ".shopping-cart-thursday"
            break;
        case "Friday":
            cartClass = ".shopping-cart-friday"
            break;
        case "Saturday":
            cartClass = ".shopping-cart-saturday"
            break;
        case "Sunday":
            cartClass = ".shopping-cart-sunday"
            break;
        default:
            break;
    }
    return cartClass
}

function translateDutchDayToEnglish(day) {
    let englishDay = ""
    switch (day) {
        case "Maandag":
            englishDay = "Monday"
            break;
        case "Dinsdag":
            englishDay = "Tuesday"
            break;
        case "Woensdag":
            englishDay = "Wednesday"
            break;
        case "Donderdag":
            englishDay = "Thursday"
            break;
        case "Vrijdag":
            englishDay = "Friday"
            break;
        case "Zaterdag":
            englishDay = "Saturday"
            break;
        case "Zondag":
            englishDay = "Sunday"
            break;
        default:
            break;
    }
    return englishDay
}
