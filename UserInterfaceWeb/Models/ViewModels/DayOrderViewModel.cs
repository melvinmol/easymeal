﻿using DomainModel;

namespace UserInterfaceWeb.Models.ViewModels
{
    public class DayOrderViewModel
    {
        public MealOrder MealOrder { get; set; }
        public Day Day { get; set; }
    }
}
