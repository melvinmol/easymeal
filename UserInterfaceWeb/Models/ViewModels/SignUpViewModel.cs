﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;

namespace UserInterfaceWeb.Models.ViewModels
{
    public class SignUpViewModel
    {
        [Required(ErrorMessage = "Email moet ingevuld worden.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Voornaam moet ingevuld worden.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Achternaam moet ingevuld worden.")]
        public string SecondName { get; set; }
        [Required(ErrorMessage = "Stad moet ingevuld worden.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Straat moet ingevuld worden.")]
        public string Street { get; set; }
        [Required(ErrorMessage = "Huisnummer moet ingevuld worden.")]
        public string HouseNumber { get; set; }
        [Required(ErrorMessage = "Geboortedatum moet ingevuld worden.")]
        public DateTime Birthday { get; set; }
        [Required(ErrorMessage = "Wachtwoord moet ingevuld worden.")]
        [RegularExpression(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            ErrorMessage = "Een wachtwoord moet de volgende tekens bevatten: een kleine letter, een grote letter, " +
                           "een cijfer, een speciaal teken(#?!@$%^&*-) en moet minimaal 8 karakters lang zijn")]
        public string Password { get; set; }
        public IList<DietaryRestriction> DietaryRestrictions { get; set; }
    }
}
