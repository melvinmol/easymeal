﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserInterfaceWeb.Models.ViewModels
{
    public class MealCardViewModel
    {
        public MealType MealType { get; set; }
        public WeekMenu WeekMenu { get; set; }
    }
}
