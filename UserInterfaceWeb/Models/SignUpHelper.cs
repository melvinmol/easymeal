﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Identity;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public static class SignUpHelper
    {
        /*
         * SignUp IdentityUser
         * Checks if IdentityCuster, if not returns false
         * Creates Customer in database from SignUpViewModel
         * Param: SignUpViewModel with all Customer attributes
         * Param: IdentityUser
         * Param: SignInManager
         * Param: CustomerDAO
         * Returns: true
         */
        public static async Task<bool> SignUp(SignUpViewModel signUpViewModel, UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager, ICustomerDAO customerDAO)
        {
            //Creates IdentityUser
            IdentityUser user = new IdentityUser(signUpViewModel.Email);
            var createdUser = await userManager.CreateAsync(user, signUpViewModel.Password);
            if (createdUser.Succeeded)
            {
                await customerDAO.Create(new Customer()
                {
                    Email = signUpViewModel.Email,
                    FirstName = signUpViewModel.FirstName,
                    SecondName = signUpViewModel.SecondName,
                    City = signUpViewModel.City,
                    Street = signUpViewModel.Street,
                    HouseNumber = signUpViewModel.HouseNumber,
                    Birthday = signUpViewModel.Birthday,
                    DietaryRestrictions = signUpViewModel.DietaryRestrictions
                });
                return true;
            }
            return false;
        }
    }
}
