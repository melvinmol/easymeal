﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public class DayOrderViewModelCart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual void AddItem(DayOrderViewModel dayOrderViewModel, int quantity)
        {
            CartLine line = lineCollection
                .FirstOrDefault(m =>
                    m.DayOrderViewModel.MealOrder.MealId == dayOrderViewModel.MealOrder.MealId
                    && m.DayOrderViewModel.Day == dayOrderViewModel.Day
                    && m.DayOrderViewModel.MealOrder.Size == dayOrderViewModel.MealOrder.Size);

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    DayOrderViewModel = dayOrderViewModel,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public virtual void RemoveLine(DayOrderViewModel dayOrderViewModel) =>
            lineCollection.RemoveAll((m =>
                    m.DayOrderViewModel.MealOrder.MealId == dayOrderViewModel.MealOrder.MealId
                    && m.DayOrderViewModel.Day == dayOrderViewModel.Day
                    && m.DayOrderViewModel.MealOrder.Size == dayOrderViewModel.MealOrder.Size));

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public DayOrderViewModel DayOrderViewModel { get; set; }
        public int Quantity { get; set; }
    }
}
