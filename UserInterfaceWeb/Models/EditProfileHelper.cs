﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public class EditProfileHelper
    {
        public static async Task<SignUpViewModel> IdentityUserToSignUpViewModel(IdentityUser user,
            ICustomerDAO customerDAO)
        {
            Customer customer = await customerDAO.ReadByEmail(user.UserName);
            return new SignUpViewModel()
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                SecondName = customer.SecondName,
                City = customer.City,
                Street = customer.Street,
                HouseNumber = customer.HouseNumber,
                Birthday = customer.Birthday,
                DietaryRestrictions = customer.DietaryRestrictions
            };
        }

        /*
         * Creates Customer in database from SignUpViewModel
         * Param: SignUpViewModel with all Customer attributes
         * Param: CustomerDAO
         */
        public static async Task UpdateCustomer(SignUpViewModel signUpViewModel, ICustomerDAO customerDAO)
        {
            await customerDAO.Update(new Customer()
            {
                Email = signUpViewModel.Email,
                FirstName = signUpViewModel.FirstName,
                SecondName = signUpViewModel.SecondName,
                City = signUpViewModel.City,
                Street = signUpViewModel.Street,
                HouseNumber = signUpViewModel.HouseNumber,
                Birthday = signUpViewModel.Birthday,
                DietaryRestrictions = signUpViewModel.DietaryRestrictions
            });
        }
    }
}
