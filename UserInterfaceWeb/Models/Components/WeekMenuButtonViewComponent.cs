﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using DomainServices.Repositories;
using UserInterfaceWeb.Models;
using Microsoft.AspNetCore.Identity;

namespace ChefUserInterfaceWeb.Models.Components
{
    public class WeekMenuButtonViewComponent : ViewComponent
    {
        private IWeekMenuDAO weekMenuDAO;
        private IWeekOrderDAO weekOrderDAO;
        private UserManager<IdentityUser> userManager;

        public WeekMenuButtonViewComponent(IWeekMenuDAO weekMenuDAO, IWeekOrderDAO weekOrderDAO, UserManager<IdentityUser> userManager)
        {
            this.weekMenuDAO = weekMenuDAO;
            this.weekOrderDAO = weekOrderDAO;
            this.userManager = userManager;
        }

        public IViewComponentResult Invoke()
        {
            IdentityUser user = userManager.GetUserAsync(HttpContext.User).Result;

            IList<WeekOrder> weekOrders = weekOrderDAO.ReadByCustomerEmail(user.UserName).Result;
            IList<WeekMenu> weekMenus = weekMenuDAO.Read().Result;

            IList<WeekMenu> weeksNotOrdered = WeeksOrderHelper.GetNotOrderedWeeks(weekMenus, weekOrders);

            return View(weeksNotOrdered);
        }
    }
}
