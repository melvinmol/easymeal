﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using DomainServices.Repositories;
using UserInterfaceWeb.Models;
using Microsoft.AspNetCore.Identity;

namespace ChefUserInterfaceWeb.Models.Components
{
    public class InvoiceButtonViewComponent : ViewComponent
    {
        private IWeekOrderDAO weekOrderDAO;
        private UserManager<IdentityUser> userManager;

        public InvoiceButtonViewComponent(IWeekOrderDAO weekOrderDAO, UserManager<IdentityUser> userManager)
        {
            this.weekOrderDAO = weekOrderDAO;
            this.userManager = userManager;
        }

        public IViewComponentResult Invoke()
        {
            IdentityUser user = userManager.GetUserAsync(HttpContext.User).Result;

            List<WeekOrder> weekOrders = weekOrderDAO.ReadByCustomerEmail(user.UserName).Result;

            IList<MealMonthInvoice> mealMonthInvoices = MealMonthInvoiceMaker.MakeMealMonthInvoices(weekOrders);

            return View(mealMonthInvoices);
        }
    }
}
