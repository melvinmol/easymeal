﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
using DomainModel;
using Microsoft.AspNetCore.Mvc;
using UserInterfaceWeb.Models.ViewModels;
using DomainServices.Repositories;

namespace ChefUserInterfaceWeb.Models.Components
{
    public class MealCardViewComponent : ViewComponent
    {
        private IMealDAO mealDAO;

        public MealCardViewComponent(IMealDAO mealDAO)
        {
            this.mealDAO = mealDAO;
        }

        public IViewComponentResult Invoke(MealCardViewModel mealCardViewModel)
        {
            IList<Meal> listMeal = mealDAO.ReadByWeekMenuId(mealCardViewModel.WeekMenu.Id).Result;

            mealCardViewModel.WeekMenu.Meals = listMeal;

            return View(mealCardViewModel.WeekMenu.Meals.Where<Meal>(meal => meal.MealType == mealCardViewModel.MealType).ToList());
        }
    }
}
