﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public class SessionDayOrderViewModelCart : DayOrderViewModelCart
    {

        public static DayOrderViewModelCart GetDayOrderViewModelCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;
            SessionDayOrderViewModelCart cart = session?.GetJson<SessionDayOrderViewModelCart>("DayOrderViewModelCart")
                               ?? new SessionDayOrderViewModelCart();
            cart.Session = session;
            return cart;
        }
        [JsonIgnore]
        public ISession Session { get; set; }
        public override void AddItem(DayOrderViewModel dayOrderViewModel, int quantity)
        {
            base.AddItem(dayOrderViewModel, quantity);
            Session.SetJson("DayOrderViewModelCart", this);
        }
        public override void RemoveLine(DayOrderViewModel dayOrderViewModel)
        {
            base.RemoveLine(dayOrderViewModel);
            Session.SetJson("DayOrderViewModelCart", this);
        }
        public override void Clear()
        {
            base.Clear();
            Session.Remove("DayOrderViewModelCart");
        }
    }
}
