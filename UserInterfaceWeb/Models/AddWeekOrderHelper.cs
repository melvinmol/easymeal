﻿using DomainModel;
using DomainModel.Models;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserInterfaceWeb.Models.ViewModels;

namespace UserInterfaceWeb.Models
{
    public static class AddWeekOrderHelper
    {
        public static async Task<IList<string>> AddWeekOrder(
            DayOrderViewModelCart dayOrderViewModelCart,
            WeekMenu weekMenu,
            IWeekOrderDAO weekOrderDAO,
            ICustomerDAO customerDAO,
            IdentityUser user)
        {
            WeekOrder weekOrder = new WeekOrder();

            weekOrder.WeekMenuId = weekMenu.Id;
            weekOrder.BeginDate = weekMenu.BeginDate;
            weekOrder.EndDate = weekMenu.EndDate;

            weekOrder.Customer = await customerDAO.ReadByEmail(user.UserName);

            foreach (CartLine cartLine in dayOrderViewModelCart.Lines)
            {
                for (int counter = 0; counter < cartLine.Quantity; counter++)
                {
                    Day day = cartLine.DayOrderViewModel.Day;
                    if (weekOrder.DayOrders.Any<DayOrder>(dayOrder => dayOrder.Day == day))
                    {
                        weekOrder
                            .DayOrders
                            .FirstOrDefault<DayOrder>(dayOrder =>
                            dayOrder.Day == day)
                            .MealOrders
                            .Add(cartLine.DayOrderViewModel.MealOrder);
                    }
                    else
                    {
                        DayOrder dayOrder = new DayOrder()
                        {
                            Day = day
                        };
                        dayOrder.MealOrders.Add(cartLine.DayOrderViewModel.MealOrder);
                        weekOrder.DayOrders.Add(dayOrder);
                    }
                }
            }
            IList<string> validations = new List<string>();

            if (!MinimalDayOrderChecker.EveryDayHasAMinimalTwoMeals(weekOrder))
            {
                validations.Add("Voor iedere dag moet er minimaal een hoofdgerecht besteld zijn en één extra gerecht");
            }

            if (!MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrder))
            {
                validations.Add("Er moet voor minimaal vier werkdagen besteld zijn, het weekend is optioneel");
            }

            if (validations.Count == 0)
            {
                await weekOrderDAO.Create(weekOrder);
            }
            return validations;
        }
    }
}
