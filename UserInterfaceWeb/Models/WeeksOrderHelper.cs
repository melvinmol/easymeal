﻿using DomainModel;
using System.Collections.Generic;
using System.Linq;

namespace UserInterfaceWeb.Models
{
    public static class WeeksOrderHelper
    {
        public static IList<WeekMenu> GetNotOrderedWeeks(IList<WeekMenu> weekMenus, IList<WeekOrder> weekOrders)
        {
            IList<WeekMenu> weeksNotOrdered = new List<WeekMenu>();

            foreach(WeekMenu weekMenu in weekMenus)
            {
                if (!weekOrders.Any<WeekOrder>(weekOrder => weekOrder?.WeekMenuId == weekMenu?.Id))
                {
                    weeksNotOrdered.Add(weekMenu);
                }
            }
            return weeksNotOrdered;
        } 
    }
}
