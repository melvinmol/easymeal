﻿using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserInterfaceWeb.Models
{
    public static class InvoiceHelper
    {
        public async static Task<MealMonthInvoice> FindMealMonthInvoice(
            IWeekOrderDAO weekOrderDAO, 
            MealMonthInvoice mealMonthInvoice, 
            IdentityUser user, 
            ICustomerDAO customerDAO)
        {

            List<WeekOrder> weekOrders = await weekOrderDAO.ReadByCustomerEmail(user.UserName);
            IList<MealMonthInvoice> mealMonthInvoices = MealMonthInvoiceMaker.MakeMealMonthInvoices(weekOrders);
            MealMonthInvoice found = mealMonthInvoices
                .FirstOrDefault<MealMonthInvoice>(mealMonthInvoiceSearch =>
                mealMonthInvoiceSearch
                .Month
                .Equals(mealMonthInvoice.Month) && mealMonthInvoiceSearch
                .Year
                .Equals(mealMonthInvoice.Year));
            BirthDayChecker.Give10PerCentDiscountWhenBirthDay(await customerDAO.ReadByEmail(user.UserName), found);
            PriceHelper.CalculatePriceMealMontInvoice(found);
            return found;
        }
    }
}
