﻿using DomainServices.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using OrderDataLayer.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using IdentityDataLayer;
using OrderDataLayer;
using Microsoft.AspNetCore.Http;
using UserInterfaceWeb.Models;

namespace UserInterfaceWeb
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment env { get; }

        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            this.env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var orderDbConnectionString = Configuration["easy-meal-orderdb:ConnectionString"];
            var identityDbConnectionString = Configuration["easy-meal-identitydb:ConnectionString"];

            if (env.IsDevelopment())
            {
                orderDbConnectionString = Configuration["easy-meal-orderdbLocal:ConnectionString"];
                identityDbConnectionString = Configuration["easy-meal-identitydbLocal:ConnectionString"];
            }
            services.AddDbContext<OrderDbContext>(options =>
                options.UseSqlServer(orderDbConnectionString));

            services.AddDbContext<IdentityDbContext>(options =>
                options.UseSqlServer(identityDbConnectionString));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<ICustomerDAO, SQLCustomerDAO>();
            services.AddTransient<IWeekOrderDAO, SQLWeekOrderDAO>();
            services.AddTransient<IWeekMenuDAO, APIWeekMenuDAO>();
            services.AddTransient<IMealDAO, APIMealDAO>();

            services.AddSession();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<DayOrderViewModelCart>(dayOrderViewModelCart =>
                SessionDayOrderViewModelCart.GetDayOrderViewModelCart(dayOrderViewModelCart));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvcWithDefaultRoute();
        }
    }
}
