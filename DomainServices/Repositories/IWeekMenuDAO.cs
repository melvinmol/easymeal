﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface IWeekMenuDAO
    {
        Task Create(WeekMenu weekMenu);
        Task<List<WeekMenu>> Read();
        Task<List<WeekMenu>> ReadWithoutMeals();
        Task<WeekMenu> ReadById(int id);
        Task Update(WeekMenu weekMenu);
        Task Delete(int id);
    }
}
