﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface IDayOrderDAO
    {
        Task Create(DayOrder dayOrder);
        Task<List<DayOrder>> Read();
        Task<DayOrder> ReadById(int id);
        Task Update(DayOrder dayOrder);
        Task Delete(int dayOrder);
    }
}
