﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface IChefDAO
    {
        Task Create(Chef chef);
        Task<List<Chef>> Read();
        Task<Chef> ReadByEmail(string email);
        Task Update(Chef chef);
        Task Delete(string email);
    }
}