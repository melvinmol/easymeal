﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface IMealDAO
    {
        Task Create(Meal meal);
        Task<List<Meal>> Read();
        Task<Meal> ReadById(int id);
        Task<List<Meal>> ReadByWeekMenuId(int id);
        Task Update(Meal meal);
        Task Delete(int id);
    }
}
