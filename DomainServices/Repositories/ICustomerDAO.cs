﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface ICustomerDAO
    {
        Task Create(Customer customer);
        Task<List<Customer>> Read();
        Task<Customer> ReadByEmail(string email);
        Task Update(Customer customer);
        Task Delete(string email);
    }
}
