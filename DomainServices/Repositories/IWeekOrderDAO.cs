﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DomainServices.Repositories
{
    public interface IWeekOrderDAO
    {
        Task Create(WeekOrder weekOrder);
        Task<List<WeekOrder>> Read();
        Task<List<WeekOrder>> ReadByCustomerEmail(string email);
        Task<WeekOrder> ReadById(int id);
        Task Update(WeekOrder weekOrder);
        Task Delete(int id);
    }
}
