﻿using MealAPILevel2;
using DomainModel;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using MealAPILevel2.Controllers;

namespace EasyMeal.Test
{
    public class MenuControllerTests
    {
        [Fact]
        public void MenuIndexShouldReturnListOfMenus()
        {
            // Arrange - create the mock repository
            Mock<IWeekMenuDAO> mockWeekMenu = new Mock<IWeekMenuDAO>();
            List<WeekMenu> weekMenus = new List<WeekMenu>
            {
                new WeekMenu { Id = 2}
            };
            mockWeekMenu.Setup(m => m.ReadWithoutMeals()).Returns(Task.FromResult(weekMenus));

            Mock<IMealDAO> mockMeal = new Mock<IMealDAO>();
            Mock<IChefDAO> mockChef = new Mock<IChefDAO>();

            // Arrange - create a controller
            MenusController target = new MenusController(
                mockWeekMenu.Object,
                mockMeal.Object
                );

            // Action
            IActionResult result = target.Get();
            var okResult = result as OkObjectResult;
            IList<WeekMenu> weekMenusEndResult = okResult.Value as List<WeekMenu>;

            // Assert
            Assert.Equal(1, weekMenusEndResult.Count);
            Assert.Equal(2, weekMenusEndResult.First().Id);
        }
    }
}
