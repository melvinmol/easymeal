﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using System.Linq;
using DomainModel.Models;

namespace EasyMeal.Tests
{
    public class AllDietsCheckerTests
    {
        [Fact]
        public void AllDietsCheckerShouldReturnTrueWhenWeekMenuHasAllDietaryRestrictions()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, 
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" },
                    new DietaryRestriction() { Name = "WithoutGuten" },
                    new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void AllDietsCheckerShouldReturnFalseWhenWeekMenuHasNotDiabets()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() {
                    new DietaryRestriction() { Name = "WithoutGuten" },
                    new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void AllDietsCheckerShouldReturnFalseWhenWeekMenuHasNotWithoutGuten()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() {
                    new DietaryRestriction() { Name = "Diabetes" },
                    new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutSalt" } } },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void AllDietsCheckerShouldReturnFalseWhenWeekMenuHasNotWithoutSalt()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() {
                    new DietaryRestriction() { Name = "Diabetes" },
                    new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "Diabetes" } } }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void AllDietsCheckerShouldReturnFalseWhenWeekMenuHasNotWithoutSaltAndDiabets()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() {
                    new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() { new DietaryRestriction() { Name = "WithoutGuten" } } }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void AllDietsCheckerShouldReturnFalseWhenWeekMenuHasNoDietaryRestrictions()
        {
            // Arrange
            List<Meal> fakeMealList = new List<Meal> {
                new Meal { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal3", Description = "Description 3", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal4", Description = "Description 4", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal5", Description = "Description 5", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal6", Description = "Description 6", MealType = MealType.Dessert,
                    Diets = new List<DietaryRestriction>() },
                new Meal { Id = 1, Price = 5, Name = "meal7", Description = "Description 7", MealType = MealType.Starter,
                    Diets = new List<DietaryRestriction>() },

                new Meal { Id = 1, Price = 5, Name = "meal8", Description = "Description 8", MealType = MealType.MainDish,
                    Diets = new List<DietaryRestriction>() }
            };

            Mock<WeekMenu> weekMenuMock = new Mock<WeekMenu>();
            weekMenuMock.Object.Meals = fakeMealList;

            // Act
            bool result = AllDietsChecker.WeekMenuHasAllDiets(weekMenuMock.Object);

            // Assert
            Assert.False(result);
        }
    }
}
