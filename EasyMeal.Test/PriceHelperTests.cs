﻿using DomainModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EasyMeal.Tests
{
    public class PriceHelperTests
    {
        [Fact]
        public void CalculatePriceMealMontInvoiceShouldReturn10PerCentLessWhen18MealsOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList2 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList3 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            List<DayOrder> dayOrders2 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList2 } };

            List<DayOrder> dayOrders3 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList3 } };

            Customer customer = new Customer()
            {
                FirstName = "FirstName",
                SecondName = "SecondName",
                Birthday = Convert.ToDateTime("05/05/2005")
            };

            List<WeekOrder> weekOrders = new List<WeekOrder>() {
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders2
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders3
                }
            };


            MealMonthInvoice mealMonthInvoice = new MealMonthInvoice()
            {
                WeekOrders = weekOrders,
                IsBirthDay = false
            };

            // Act
            PriceHelper.CalculatePriceMealMontInvoice(mealMonthInvoice);
            bool result = mealMonthInvoice.TotalCosts == 81;

            // Assert
            Assert.True(result);
            Assert.True(mealMonthInvoice.HasPriceDiscount);
        }

        [Fact]
        public void CalculatePriceMealMontInvoiceShouldNotReturn10PerCentLessWhen15MealsOrderedAndHasBirthDayDiscount()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList2 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList3 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            List<DayOrder> dayOrders2 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList2 } };

            List<DayOrder> dayOrders3 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList3 } };

            Customer customer = new Customer()
            {
                FirstName = "FirstName",
                SecondName = "SecondName",
                Birthday = Convert.ToDateTime("05/05/2005")
            };

            List<WeekOrder> weekOrders = new List<WeekOrder>() {
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders2
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders3
                }
            };

            MealMonthInvoice mealMonthInvoice = new MealMonthInvoice()
            {
                WeekOrders = weekOrders,
                IsBirthDay = true
            };

            // Act
            PriceHelper.CalculatePriceMealMontInvoice(mealMonthInvoice);
            bool result = mealMonthInvoice.TotalCosts == 75;

            // Assert
            Assert.True(result);
            Assert.False(mealMonthInvoice.HasPriceDiscount);
        }

        [Fact]
        public void CalculatePriceMealMontInvoiceShouldReturn10PerCentLessWhen15MealsOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList2 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList3 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            List<DayOrder> dayOrders2 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList2 } };

            List<DayOrder> dayOrders3 = new List<DayOrder> { new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList3 } };

            Customer customer = new Customer()
            {
                FirstName = "FirstName",
                SecondName = "SecondName",
                Birthday = Convert.ToDateTime("05/05/2005")
            };

            List<WeekOrder> weekOrders = new List<WeekOrder>() {
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders2
                },
                new WeekOrder() {
                    Customer = customer,
                    DayOrders = dayOrders3
                }
            };

            MealMonthInvoice mealMonthInvoice = new MealMonthInvoice()
            {
                WeekOrders = weekOrders,
                IsBirthDay = false
            };

            // Act
            PriceHelper.CalculatePriceMealMontInvoice(mealMonthInvoice);
            bool result = mealMonthInvoice.TotalCosts == 68;

            // Assert
            Assert.True(result);
            Assert.True(mealMonthInvoice.HasPriceDiscount);
        }
    }
}
