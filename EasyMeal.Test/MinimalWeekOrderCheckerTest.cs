﻿using DomainModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EasyMeal.Tests
{
    public class MinimalWeekOrderCheckerTests
    {
        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnTrueWhenAllDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { 
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Thursday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Tuesday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnTrueWhenFourDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Thursday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Tuesday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenFiveDayOrderedBeTwoOfThemAreInTheWeekend()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Thursday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenFourDayOrderedBeOneOfThemAreInTheWeekend()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Thursday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenThreeDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Thursday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenTwoDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenOneDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenNoDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder>();

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void WeekOrderHasMinimalFourDayOrderedShouldReturnFalseWhenSaterDayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Wednesday, MealOrders = fakeMealOrderList }
            };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalWeekOrderChecker.WeekOrderHasMinimalFourDayOrdered(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }
    }
}
