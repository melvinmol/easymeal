﻿using DomainModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EasyMeal.Tests
{
    public class MinimalDayOrderCheckerTests
    {
        [Fact]
        public void EveryDayHasAMinimalTwoMealsShouldReturnTrueWhenDayOrderHasAMainDishAndADessert()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.MainDish, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalDayOrderChecker.EveryDayHasAMinimalTwoMeals(weekOrderMock.Object);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void EveryDayHasAMinimalTwoMealsShouldReturnFalseWhenDayOrderHasOnlyADesser()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalDayOrderChecker.EveryDayHasAMinimalTwoMeals(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void EveryDayHasAMinimalTwoMealsShouldReturnFalseWhenDayOrderHasADessertAndStarter()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 1, Price = 5, Name = "meal2", Description = "Description 1", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalDayOrderChecker.EveryDayHasAMinimalTwoMeals(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void EveryDayHasAMinimalTwoMealsShouldReturnFalseWhenDayOrderHasOnlyStarter()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            Mock<WeekOrder> weekOrderMock = new Mock<WeekOrder>();
            weekOrderMock.Object.DayOrders = dayOrders;

            // Act
            bool result = MinimalDayOrderChecker.EveryDayHasAMinimalTwoMeals(weekOrderMock.Object);

            // Assert
            Assert.False(result);
        }
    }
}
