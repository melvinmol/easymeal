﻿using DomainModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EasyMeal.Tests
{
    public class WeekOrderTests
    {
        [Fact]
        public void TotalCostsShouldReturn25()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 3, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 12, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 3, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            WeekOrder weekOrder = new WeekOrder() { DayOrders = dayOrders };

            // Act
            bool result = weekOrder.TotalCosts() == 25;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void TotalCostsShouldReturn50When3DayOrdered()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 3, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 12, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 3, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList2 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 3, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.Dessert, Size = Size.Minus20 }
            };

            List<MealOrder> fakeMealOrderList3 = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 3, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 3, Price = 7, Name = "meal2", Description = "Description 2", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 5, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> {
                new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList },
                new DayOrder() { Day = Day.Monday, MealOrders = fakeMealOrderList2 },
                new DayOrder() { Day = Day.Saturday, MealOrders = fakeMealOrderList3 }
            };

            WeekOrder weekOrder = new WeekOrder() { DayOrders = dayOrders };

            // Act
            bool result = weekOrder.TotalCosts() == 50;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void TotalCostsShouldNotReturn30()
        {
            // Arrange
            List<MealOrder> fakeMealOrderList = new List<MealOrder> {
                new MealOrder { Id = 1, Price = 5, Name = "meal1", Description = "Description 1", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 3, Price = 5, Name = "meal2", Description = "Description 2", MealType = MealType.Dessert, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 12, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 },
                new MealOrder { Id = 25, Price = 3, Name = "meal2", Description = "Description 3", MealType = MealType.Starter, Size = Size.Minus20 }
            };

            List<DayOrder> dayOrders = new List<DayOrder> { new DayOrder() { Day = Day.Friday, MealOrders = fakeMealOrderList } };

            WeekOrder weekOrder = new WeekOrder() { DayOrders = dayOrders };

            // Act
            bool result = weekOrder.TotalCosts() == 30;

            // Assert
            Assert.False(result);
        }
    }
}
