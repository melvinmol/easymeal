﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DomainModel
{
    public static class MealMonthInvoiceMaker
    {
        public static IList<MealMonthInvoice> MakeMealMonthInvoices(List<WeekOrder> weekOrders)
        {
            IList<MealMonthInvoice> mealMonthInvoices = new List<MealMonthInvoice>();

            weekOrders.Sort((x, y) => DateTime.Compare(x.BeginDate, y.BeginDate));

            string month = "";
            string year = "";


            foreach (WeekOrder weekOrder in weekOrders)
            {
                string newMonth = weekOrder.BeginDate.Month.ToString();
                string newYear = DateTime.Now.Year.ToString();
                if (month.Equals(newMonth) && year.Equals(newYear))
                {
                    MealMonthInvoice founded = mealMonthInvoices
                        .FirstOrDefault<MealMonthInvoice>(mealMonthInvoice =>
                        mealMonthInvoice
                        .Month
                        .Equals(newMonth) && mealMonthInvoice.Year.Equals(newYear));

                    founded.WeekOrders.Add(weekOrder);
                } else
                {
                    MealMonthInvoice mealMonthInvoice = new MealMonthInvoice();
                    mealMonthInvoice.Month = newMonth;
                    mealMonthInvoice.Year = newYear;

                    mealMonthInvoice.WeekOrders.Add(weekOrder);

                    year = newYear;
                    month = newMonth;
                    mealMonthInvoices.Add(mealMonthInvoice);
                }
            }
            return mealMonthInvoices;
        }
    }
}
