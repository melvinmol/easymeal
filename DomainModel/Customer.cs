﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainModel
{
    public class Customer : AbstractPerson
    {
        public int CustomerNumber { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public DateTime Birthday { get; set; }
        public List<WeekOrder> WeekOrders { get; set; }
        public IList<DietaryRestriction> DietaryRestrictions { get; set; }

        public Customer()
        {
            this.CustomerNumber = GenerateNumber.RandomNumber();
        }
    }
}
