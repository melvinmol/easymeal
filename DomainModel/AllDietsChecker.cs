﻿using DomainModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DomainModel.Models
{
    public static class AllDietsChecker
    {
        public static Boolean WeekMenuHasAllDiets(WeekMenu weekMenu)
        {
            bool withoutSalt = false;
            bool diabetes = false;
            bool withoutGuten = false;

            foreach (Meal meal in weekMenu.Meals)
            {
                if (withoutSalt && diabetes && withoutGuten)
                {
                    break;
                }

                foreach (DietaryRestriction dietaryRestriction in meal.Diets)
                {
                    if (dietaryRestriction.Name.Equals("WithoutSalt"))
                    {
                        withoutSalt = true;
                    }
                    else if (dietaryRestriction.Name.Equals("Diabetes"))
                    {
                        diabetes = true;
                    }
                    else if (dietaryRestriction.Name.Equals("WithoutGuten"))
                    {
                        withoutGuten = true;
                    }
                }
            }

            if (withoutSalt && diabetes && withoutGuten)
            {
                return true;

            }
            return false;
        }
    }
}
