﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainModel
{
    public class DayOrder
    {
        public int Id { get; set; }
        public int WeekOrderId { get; set; }
        public Day Day { get; set; }
        public IList<MealOrder> MealOrders { get; set; } = new List<MealOrder>();
    }
}