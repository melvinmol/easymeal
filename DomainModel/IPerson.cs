﻿namespace DomainModel
{
    public interface IPerson
    {
        string FirstName { get; set; }
        string SecondName { get; set; }
        string Email { get; set; }
        string FullName();
    }
}
