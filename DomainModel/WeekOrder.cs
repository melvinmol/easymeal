﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainModel
{
    public class WeekOrder
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public IList<DayOrder> DayOrders { get; set; } = new List<DayOrder>();
        public int WeekMenuId { get; set; }

        public double TotalCosts()
        {
            double costs = 0;
            foreach(DayOrder dayOrder in DayOrders)
            {
                foreach(MealOrder mealOrder in dayOrder.MealOrders)
                {
                    costs += mealOrder.Price;
                }
            }
            return costs;
        }
    }
}
