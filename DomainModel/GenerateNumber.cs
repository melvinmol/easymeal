﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public static class GenerateNumber
    {
        public static int RandomNumber()
        {
            Random random = new Random();
            return random.Next(0, 1000000000);
        }
    }
}
