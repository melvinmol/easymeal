﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DomainModel
{
    public abstract class AbstractPerson : IPerson
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public string FullName()
        {
            return FirstName + " " + SecondName;
        }
    }
}
