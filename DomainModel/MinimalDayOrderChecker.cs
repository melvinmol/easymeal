﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public static class MinimalDayOrderChecker
    {

        public static Boolean EveryDayHasAMinimalTwoMeals(WeekOrder weekOrder)
        {
            int monday = 0;
            int tuesday = 0;
            int wednesday = 0;
            int thursday = 0;
            int friday = 0;
            int saturday = 0;
            int sunday = 0;

            foreach (DayOrder dayOrder in weekOrder.DayOrders)
            {
                switch (dayOrder.Day)
                {
                    case Day.Monday:
                        monday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Tuesday:
                        tuesday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Wednesday:
                        wednesday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Thursday:
                        thursday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Friday:
                        friday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Saturday:
                        saturday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    case Day.Sunday:
                        sunday = CheckIfDayOrderHasMainDishAndOneExtra(dayOrder);
                        break;
                    default:
                        break;
                }
            }

            if (
                monday == 1 ||
                tuesday == 1 ||
                wednesday == 1 ||
                thursday == 1 ||
                friday == 1 ||
                saturday == 1 ||
                sunday == 1
                )
            {
                return false;
            }
            return true;
        }

        private static int CheckIfDayOrderHasMainDishAndOneExtra(DayOrder dayOrder)
        {
            int starter = 0;
            int mainDish = 0;
            int dessert = 0;

            foreach (MealOrder mealOrder in dayOrder.MealOrders)
            {
                if (mealOrder.MealType == MealType.Starter)
                {
                    if (starter < 1)
                    {
                        starter++;
                    }
                }
                else if (mealOrder.MealType == MealType.MainDish)
                {
                    if (mainDish < 1)
                    {
                        mainDish++;
                    }
                }
                else if (mealOrder.MealType == MealType.Dessert)
                {
                    if (dessert < 1)
                    {
                        dessert++;
                    }
                }
            }

            if (mainDish == 0)
            {
                return 1;
            }

            return starter + mainDish + dessert;
        }
    }
}
