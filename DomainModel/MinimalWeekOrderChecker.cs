﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public static class MinimalWeekOrderChecker
    {
        public static Boolean WeekOrderHasMinimalFourDayOrdered(WeekOrder weekOrder)
        {
            bool monday = false;
            bool tuesday = false;
            bool wednesday = false;
            bool thursday = false;
            bool friday = false;

            foreach (DayOrder dayOrder in weekOrder.DayOrders)
            {
                switch (dayOrder.Day)
                {
                    case Day.Monday:
                        monday = true;
                        break;
                    case Day.Tuesday:
                        tuesday = true;
                        break;
                    case Day.Wednesday:
                        wednesday = true;
                        break;
                    case Day.Thursday:
                        thursday = true;
                        break;
                    case Day.Friday:
                        friday = true;
                        break;
                    default:
                        break;
                }
            }
            int counter = 0;
            if (monday)
            {
                counter++;
            }
            if (tuesday)
            {
                counter++;
            }
            if (wednesday)
            {
                counter++;
            }
            if (thursday)
            {
                counter++;
            }
            if (friday)
            {
                counter++;
            }

            if (counter >= 4)
            {
                return true;
            }
            return false;
        }
    }
}
