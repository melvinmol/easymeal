﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace DomainModel
{
    public class MealOrder
    {
        public int Id { get; set; }
        public int MealId { get; set; }
        public int DayOrderId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public MealType MealType { get; set; }
        public Size Size { get; set; }
    }
}
