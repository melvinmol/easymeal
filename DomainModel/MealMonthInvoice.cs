﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class MealMonthInvoice
    {
        public IList<WeekOrder> WeekOrders { get; set; } = new List<WeekOrder>();
        public string Month { get; set; }
        public string Year { get; set; }
        public double TotalCosts { get; set;  }
        public bool IsBirthDay { get; set; }
        public bool HasPriceDiscount { get; set; }
        public int TotalMeals { get; set; }
    }
}
