﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public static class BirthDayChecker
    {
        public static void Give10PerCentDiscountWhenBirthDay(Customer customer, MealMonthInvoice mealMonthInvoice)
        {
            if (customer.Birthday.Month.ToString().Equals(mealMonthInvoice.Month))
            {
                int day = customer.Birthday.Day;
                foreach (WeekOrder weekOrder in mealMonthInvoice.WeekOrders)
                {

                    foreach (DayOrder dayOrder in weekOrder.DayOrders)
                    {
                        int dateChecker = weekOrder.BeginDate.Day;
                        switch (dayOrder.Day)
                        {
                            case Day.Monday:
                                break;
                            case Day.Tuesday:
                                dateChecker += 1;
                                break;
                            case Day.Wednesday:
                                dateChecker += 2;
                                break;
                            case Day.Thursday:
                                dateChecker += 3;
                                break;
                            case Day.Friday:
                                dateChecker += 4;
                                break;
                            case Day.Saturday:
                                dateChecker += 5;
                                break;
                            case Day.Sunday:
                                dateChecker += 6;
                                break;
                            default:
                                break;
                        }
                        if(dateChecker == day)
                        {
                            mealMonthInvoice.IsBirthDay = true;
                            foreach (MealOrder mealOrder in dayOrder.MealOrders)
                            {
                                mealOrder.Price = 0;
                            }
                            return;
                        }
                    }
                }
            }
        }
    }
}
