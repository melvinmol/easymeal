﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public static class PriceHelper
    {
        public static void CalculatePriceMealMontInvoice(MealMonthInvoice mealMonthInvoice)
        {
            double price = 0;
            int counter = 0;
            if(mealMonthInvoice.IsBirthDay)
            {
                counter--;
            }
            foreach (WeekOrder weekOrder in mealMonthInvoice.WeekOrders)
            {
                foreach (DayOrder dayOrder in weekOrder.DayOrders)
                {
                    foreach(MealOrder mealOrder in dayOrder.MealOrders)
                    {
                        price += mealOrder.Price;
                        counter++;
                    }
                }
            }
            if(counter >= 15)
            {
                price = Math.Round(price * 0.90);
                mealMonthInvoice.HasPriceDiscount = true;
            } else
            {
                mealMonthInvoice.HasPriceDiscount = false;
            }
            mealMonthInvoice.TotalCosts = price;
        }
    }
}
