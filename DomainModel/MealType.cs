﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public enum MealType
    {
        Starter, MainDish, Dessert
    }
}
