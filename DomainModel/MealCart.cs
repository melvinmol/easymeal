﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainModel
{
    public class MealCart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public virtual void AddItem(Meal meal, int quantity)
        {
            CartLine line = lineCollection
                .FirstOrDefault(m => m.Meal.Id == meal.Id);

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Meal = meal,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public virtual void RemoveLine(Meal meal) =>
            lineCollection.RemoveAll(l => l.Meal.Id == meal.Id);

        public virtual double ComputeTotalValue() =>
            lineCollection.Sum(e => e.Meal.Price * Convert.ToDouble(e.Quantity));

        public virtual void Clear() => lineCollection.Clear();

        public virtual IEnumerable<CartLine> Lines => lineCollection;
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public Meal Meal { get; set; }
        public int Quantity { get; set; }
    }
}
