﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainModel
{
    public class WeekMenu
    {
        public int Id { get; set; }
        public Chef Chef { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public IList<Meal> Meals { get; set; }
        public WeekMenu()
        {
            Meals = new List<Meal>();
        }
    }
}
